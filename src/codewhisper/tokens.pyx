from typing import Iterable

from .exceptions import WhisperException


# Base Token class

cdef class Token:
    def __init__(self, value, line, character, ignored, number):
        self.value = value
        self.line = line
        self.character = character
        self.ignored = ignored
        self.number = number
    
    def __repr__(self):
        return f"{self.__class__.__name__}({self.value!r})"

    def __eq__(self, other):
        r'''Tokens compare equal to other tokens of the same class with the same value.
            This is mostly useful for test cases.
        '''#"""#'''
        return self.__class__ == other.__class__ and self.value == other.value
    
    def __hash__(self):
        return hash((self.__class__, self.value))
    
    @staticmethod
    def match(text, position):
        r'''Called by the tokenization step, to collect a new token from the text.
            Should return the length of the value at that position, or None if this class doesn't match there.
        '''#"""#'''
        raise NotImplementedError
    
    def translate(self, scope):
        r'''Called by the translation step, to turn a Token in the parse tree into its final value.
        '''#"""#'''
        return self.value


# Base classes for some common token construction techniques

cdef class CharacterRangeToken(Token):
    r'''A Token that matches a single Unicode character within a range of code points
    '''#"""#'''
    
    first: int
    last: int
    
    @classmethod
    def match(cls, text, position):
        if cls.first <= ord(text[position]) <= cls.last:
            return 1

cdef class CharacterToken(Token):
    r'''A Token that matches any single character in its `characters` property.
    '''#"""#'''
    
    characters: str
    
    @classmethod
    def match(cls, text, position):
        if text[position] in cls.characters:
            return 1

cdef class DelimitedToken(Token):
    r'''A Token that matches something like a string or comment, with a beginning and end.
    '''#"""#'''
    
    start = None
    end = None
    escape = None
    
    @classmethod
    def match(cls, text, position):
        start = cls.start
        assert start
        
        pos = position + len(start)
        if text[position: pos] != start:
            return None
        
        end = cls.end
        escape = cls.escape
        assert end
        while True:
            nextpos = text.find(end, pos)
            if nextpos < 0:
                break
            
            if escape:
                escaped = 0
                length = len(escape)
                epos = nextpos - length
                while pos <= epos and text[epos: epos + length] == escape:
                    escaped += 1
                    epos -= length
                
                if escaped % 2:
                    pos = nextpos + len(end)
                    continue
            
            return nextpos + len(end) - position

cdef class PatternToken(Token):
    r'''A Token that matches a regular expression.
    '''#"""#'''
    
    pattern = None
    
    @classmethod
    def match(cls, text, position):
        if isinstance(cls.pattern, str):
            from re import compile
            cls.pattern = compile(cls.pattern)
        result = cls.pattern.match(text, position)
        if result is not None:
            return result.end() - position

cdef class StringToken(Token):
    r'''A Token that any of the strings in its `strings` property.
    '''#"""#'''
    
    strings: Iterable
    
    @classmethod
    def __init_subclass__(cls):
        try:
            cls._strings = sorted(cls.strings, reverse=True)
        except AttributeError:
            # Abstract subclasses might not have initialized the strings attribute.
            pass
        else:
            try:
                cls.initial = {s[0] for s in cls._strings}
            except IndexError:
                raise WhisperException(f"Empty string found in {cls.__name__}.strings")
    
    @classmethod
    def match(cls, text, position):
        start = text[position]
        if start not in cls.initial:
            return None
        for string in cls._strings:
            if string[0] != start:
                continue
            length = len(string)
            if text[position: position + length] == string:
                return length

cdef class KeywordToken(StringToken):
    r'''A StringToken that can't be followed directly by a letter or number.
    '''#"""#'''
    
    @classmethod
    def match(cls, text, position):
        start = text[position]
        if start not in cls.initial:
            return None
        for string in cls.strings:
            if string[0] != start:
                continue
            length = len(string)
            end = position + length
            if text[position: end] == string and not text[end: end + 1].isalnum():
                return length


# General-purpose tokens

cdef class CHARACTER(Token):
    r'''A Token that matches any single character.
    '''#"""#'''
    
    @staticmethod
    def match(text, position):
        return 1

cdef class EOF(Token):
    r'''Pseudo-token created at the end of the token stream.
    '''#"""#'''
    
    @staticmethod
    def match(text, position):
        if position == len(text):
            return 0


# Support methods

def join_tokens(tokens):
    r'''Recombine the list of tokens back into a string.
        Includes the ignored tokens between each pair.
        Ideal for tokens that were collected together.
    '''#"""#'''
    
    pieces = tokens[:1]
    for token in tokens[1:]:
        pieces.extend(token.ignored)
        pieces.append(token)
    return ''.join(token.value for token in pieces)
