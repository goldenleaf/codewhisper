cdef class Token:
    cdef readonly str value
    cdef readonly Py_ssize_t line
    cdef readonly Py_ssize_t character
    cdef Py_ssize_t number
    cdef readonly list ignored
