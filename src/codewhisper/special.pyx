from .parsing cimport ParseInfo, Parser, Result
from .parsing import get_parser


cdef class Sequence(Parser):
    r'''Matches each item in the sequence, in turn.
        The full sequence must match at least `minimum` times to return any results,
        and can match at most `limit` times unless `limit` is None.
        If `greedy` is False, shorter matches are returned first.
    '''#"""#'''

    cdef list sequence
    cdef Py_ssize_t minimum
    cdef Py_ssize_t limit
    cdef bint limited
    cdef bint greedy
    
    def __init__(self, *sequence, minimum, limit=None, greedy=True, suffix=None):
        parser: Parser
        
        self.sequence = [get_parser(item) for item in sequence]
        names = ', '.join(parser.name for parser in self.sequence)
        wrapped = names if names.isalnum() else f"({names})"
        self.minimum = minimum
        self.limited = limit is not None
        self.limit = limit if self.limited else 0
        self.greedy = greedy
        
        if self.limited and (limit < minimum):
            from .exceptions import RuleError
            raise RuleError(f"Sequence limit cannot be lower than its minimum: {limit} < {minimum}")
        
        if suffix is None:
            if limit is None:
                inner = f"{minimum},"
            elif limit == minimum:
                inner = f"{minimum}"
            else:
                inner = f"{minimum},{limit}"
            suffix = "{" + inner + "}"
            if not greedy:
                suffix += "?"
        
        self.name = f"{wrapped}{suffix}"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        initial = Result([], position)
        if self.minimum == 0 and not self.greedy:
            yield initial
        yield from self.parse_iteration(1, [], position, info)
        if self.minimum == 0 and self.greedy:
            yield initial
    
    def parse_iteration(self, iteration: Py_ssize_t, values: list, position: Py_ssize_t, info: ParseInfo):
        if self.limited and self.limit < iteration:
            return
        
        # Whether we want to yield Results from this iteration.
        valid: bint = self.minimum <= iteration
        
        result: Result
        new_result: Result
        
        if valid and not self.greedy:
            # First, attempt to use any result possible starting here.
            results = []
            for result in self.parse_sequence(self.sequence, position, info):
                new_result = Result(values + result.values, result.position)
                if position < result.position:
                    results.append(new_result)
                yield new_result
            
            # Second, attempt to use results that involve another iteration.
            for result in results:
                yield from self.parse_iteration(iteration + 1, result.values, result.position, info)
        else:
            results = []
            for result in self.parse_sequence(self.sequence, position, info):
                # Within each successful parse, first attempt to use results that involve another iteration.
                new_result = Result(values + result.values, result.position)
                if position < result.position:
                    yield from self.parse_iteration(iteration + 1, new_result.values, result.position, info)
                    if valid:
                        # Then use the result that ends with this parse.
                        yield new_result
                else:
                    # The sequence matches here with zero width, so it will continue to match forever.
                    # Shortcut that process by saving an empty result.
                    yield new_result
    
    cdef set initial_tokens(self, bint recursive=False):
        result: set = self.sequence_tokens(self.sequence, recursive)
        if self.minimum == 0:
            result.add(None)
        
        return result


cdef class Avoid(Parser):
    r'''An optional sequence of items, matching only if it must.
        Like a list, this will match either with or without the inner items.
        Unlike a list, it will first try to match without them.
    '''#"""#'''
    
    def __init__(self, *items):
        sequence = [get_parser(item) for item in items]
        names = ", ".join(item.name for item in sequence)
        self.name = f"{names}?" if names.isalnum() else f"({names})?"
        self.states = [[], sequence]


def Commas(*sequence, separator=',', trailing=False):
    r'''A set of items separated by commas, or by a separator of your choice.
        If `trailing` is true, the sequence may optionally end with the separator.
    '''#"""#'''
    
    result = (sequence, Star(separator, sequence))
    if trailing:
        result += ([separator],)
    return result


cdef class Consume(Parser):
    r'''Collects the sequence if it's there, but lets it be omitted if it's not.
        Equivalent to {sequence, notahead(sequence)}, but more efficient.
    '''#"""#'''
    
    cdef list sequence
    
    def __init__(self, *items):
        self.sequence = [get_parser(item) for item in items]
        names = ", ".join(item.name for item in self.sequence)
        self.name = f"{names}!" if names.isalnum() else f"({names})!"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        found = False
        for result in self.parse_sequence(self.sequence, position, info):
            yield result
            found = True
        if not found:
            yield Result([], position)
    
    cdef set initial_tokens(self, bint recursive=False):
        # This sequence can either be optional or collect the initial tokens of its sequence.
        result: set = self.sequence_tokens(self.sequence, recursive)
        result.add(None)
        return result


cdef class Each(Parser):
    r'''Collects exactly one instance of each item, in any order.
        Equivalent to a set of each permutation of the given items, but probably faster.
    '''#"""#'''
    
    def __init__(self, *states):
        self.states = [get_parser(state) for state in states]
        assert len(self.states) > 1
        names = ", ".join(state.name for state in self.states)
        self.name = f"Each({names})"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        yield from self.parse_iteration(self.states, position, info)
    
    def parse_iteration(self, states: list, position: Py_ssize_t, info: ParseInfo):
        if len(states) == 1:
            yield from states[0].parse(position, info)
            return
        
        state: Parser
        first: Result
        result: Result
        positions: set = set()
        
        for index, state in enumerate(states):
            rest = states[:index] + states[index + 1:]
            for first in state.parse(position, info):
                for result in self.parse_iteration(rest, first.position, info):
                    if result.position not in positions:
                        positions.add(result.position)
                        yield Result(first.values + result.values, result.position)
    
    cdef set initial_tokens(self, bint recursive=False):
        result: set = set()
        state: Parser
        for state in self.states:
            result.update(state.initial_tokens(recursive))
        return result


cdef class Lookahead(Parser):
    r'''Matches with zero width if the given sequence would match here.
    '''#"""#'''
    
    cdef list sequence
    
    def __init__(self, *items):
        self.sequence = [get_parser(item) for item in items]
        names = ", ".join(item.name for item in self.sequence)
        self.name = f"&{names}" if names.isalnum() else f"&({names})"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        found = False
        for result in self.parse_sequence(self.sequence, position, info):
            found = True
        if found:
            yield Result([], position)
    
    cdef set initial_tokens(self, bint recursive=False):
        # Without one of these tokens, this sequence can't parse.
        # The sequence can match zero tokens, but isn't really optional.
        return self.sequence_tokens(self.sequence, recursive)


cdef class Max(Parser):
    r'''Matches as many instances of the sequence as possible,
        even using shorter instances of the sequence if necessary.
    '''#"""#'''
    
    cdef list sequence
    
    def __init__(self, *sequence):
        self.sequence = [get_parser(item) for item in sequence]
        names = ', '.join(parser.name for parser in self.sequence)
        self.name = f"{names}++" if names.isalnum() else f"({names})++"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        empty = Result([], position)
        yield from self.parse_iteration([empty], info)
    
    def parse_iteration(self, states: list, info: ParseInfo):
        state: Result
        result: Result
        seen: set = set()
        saved: list = []
        for state in states:
            for result in self.parse_sequence(self.sequence, state.position, info):
                if state.position < result.position and result.position not in seen:
                    # Save this result for later.
                    seen.add(result.position)
                    saved.append(Result(state.values + result.values, result.position))
        
        if saved:
            # Look for results that involve another iteration.
            yield from self.parse_iteration(saved, info)
            
            # Return the results from this iteration.
            yield from saved
    
    cdef set initial_tokens(self, bint recursive=False):
        return self.sequence_tokens(self.sequence, recursive)


cdef class Notahead(Parser):
    r'''Matches with zero width if the given sequence doesn't match here.
    '''#"""#'''
    
    cdef list sequence
    
    def __init__(self, *items):
        self.sequence = [get_parser(item) for item in items]
        names = ", ".join(item.name for item in self.sequence)
        self.name = f"!{names}" if names.isalnum() else f"!({names})"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        # Tokens checked by this sequence could lead to confusing error messages.
        unexpected = info.unexpected()
        
        found = False
        for result in self.parse_sequence(self.sequence, position, unexpected):
            found = True
        if found:
            info.expect(position, self.name)
        else:
            yield Result([], position)
    
    cdef set initial_tokens(self, bint recursive=False):
        # Instead of returning every possible token class and string number, pretend that the whole notahead is optional.
        # This isn't quite correct if notahead is the only item in a sequence, but that doesn't seem useful.
        return {None}


cdef class Parallel(Parser):
    r'''Matches all of the sequences, as long as they consume the same tokens.
    '''#"""#'''
    
    def __init__(self, *sequences):
        if not sequences:
            from .exceptions import RuleError
            raise RuleError("Parallel must have at least one sequence")
        
        states = []
        names = []
        for item in sequences:
            if isinstance(item, tuple):
                parsers = [get_parser(value) for value in item]
                states.append(parsers)
                names.append('(' + ', '.join(parser.name for parser in parsers) + ')')
            elif isinstance(item, list):
                parsers = [get_parser(value) for value in item]
                states.append(parsers)
                states.append([])
                names.append('[' + ', '.join(parser.name for parser in parsers) + ']')
            else:
                parser = get_parser(item)
                states.append([parser])
                names.append(parser.name)
        self.states = states
        self.name = f"({' || '.join(names)})"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        pos: int
        sequence: list
        values: list
        result: Result
        order: list = []
        valid: set = set()
        results: dict = {}
        first: bool = True
        for sequence in self.states:
            seen = set()
            for result in self.parse_sequence(sequence, position, info):
                pos = result.position
                if pos in seen:
                    continue
                
                if first:
                    order.append(pos)
                    results[pos] = values = []
                elif pos not in valid:
                    continue
                else:
                    values = results[pos]
                values.extend(result.values)
                seen.add(pos)
            
            first = False
            valid = seen
            if not valid:
                return
        
        for pos in order:
            if pos in valid:
                yield Result(results[pos], pos)
    
    cdef set initial_tokens(self, bint recursive=False):
        # This must match every sequence.
        result: set = self.sequence_tokens(self.states[0], recursive)
        for state in self.states[1:]:
            result |= self.sequence_tokens(state, recursive)
        
        if None in result:
            return {None}
        
        return result


def Plus(*items):
    r'''Matches one or more instances of the given sequence.
    '''#"""#'''
    
    return Sequence(*items, minimum=1, limit=None, suffix='+')


cdef class Seek(Parser):
    r'''Matches zero or more tokens until the given sequence matches.
        (Include EOF as an option to seek to the end of the text.)
        The skipped tokens and anything in the sought sequence are available to the rule.
    '''#"""#'''
    
    cdef list sequence
    cdef Parser stop
    
    def __init__(self, *items, stop=None):
        self.sequence = [get_parser(item) for item in items]
        self.stop = get_parser(stop) if stop is not None else None
        names = ", ".join(item.name for item in self.sequence)
        self.name = f"... {names}" if names.isalnum() else f"... ({names})"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        result: Result
        unexpected = info.unexpected()
        
        found = False
        for pos in range(position, len(info.tokens)):
            for result in self.parse_sequence(self.sequence, pos, info):
                skipped = [(token.__class__, token) for token in info.tokens[position:pos]]
                yield Result(skipped + result.values, result.position)
                found = True
            if found:
                return
            if self.stop is not None:
                for result in self.stop.parse(pos, unexpected):
                    return
    
    cdef set initial_tokens(self, bint recursive=False):
        # Just about anything at all could match here, so always run this sequence.
        return {True}


cdef class Some(Parser):
    r'''Matches at least one of the given items, but only up to one of each.
        Similar to Each, but allows items to be omitted.
        (If zero items is permissible, wrap it in a list.)
    '''#"""#'''
    
    def __init__(self, *states):
        self.states = [get_parser(state) for state in states]
        assert len(self.states) > 1
        names = ", ".join(state.name for state in self.states)
        self.name = f"Some({names})"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        yield from self.parse_iteration(self.states, position, info)
    
    def parse_iteration(self, states: list, position: Py_ssize_t, info: ParseInfo):
        if len(states) == 1:
            yield from states[0].parse(position, info)
            return
        
        state: Parser
        first: Result
        result: Result
        positions: set = set()
        
        for index, state in enumerate(states):
            rest = states[:index] + states[index + 1:]
            for first in state.parse(position, info):
                for result in self.parse_iteration(rest, first.position, info):
                    if result.position not in positions:
                        positions.add(result.position)
                        yield Result(first.values + result.values, result.position)
                if first.position not in positions:
                    positions.add(first.position)
                    yield first
    
    cdef set initial_tokens(self, bint recursive=False):
        result: set = set()
        state: Parser
        for state in self.states:
            result.update(state.initial_tokens(recursive))
        return result


def Star(*items):
    r'''Matches zero or more instances of the given sequence.
    '''#"""#'''
    
    return Sequence(*items, minimum=0, limit=None, suffix='*')
