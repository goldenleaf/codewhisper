from .tokens cimport Token

from itertools import count


# Hands out numbers to StringParser instances,
# for slightly faster string terminal comparison.
cdef object string_numbers = count(1)

# Allows parsers to be re-used; particularly helpful for recursive rules.
cdef dict parser_cache = {}


cdef class Node:
    cdef readonly str name
    cdef readonly object values
    cdef object translator
    
    def __init__(self, name, values, translator):
        self.name = name
        self.values = values
        self.translator = translator

    def __repr__(self):
        return f"{self.__class__.__name__}({self.name}, {self.values})"
    
    def translate(self, scope):
        r'''Called by the translation step, to turn a Node in the parse tree into its final value.
        '''#"""#'''
        
        return self.translator(self.values, scope)


cdef class Result:
    def __cinit__(self, list values, Py_ssize_t position):
        # A list of (base, value) pairs, where base is either a rule function or a Token class.
        self.values = values
        
        # The number of the first token that wasn't parsed.
        self.position = position

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(values={self.values!r}, position={self.position!r})"


cdef class ParseInfo:
    r'''Storage for values used by a single parse.
    '''#"""#'''
    
    def __init__(self, tokens: list, verbose: bint, cache: dict):
        self.tokens = tokens
        self.verbose = verbose
        self.cache = cache
        self.last_position = 0
        self.expected = set()
    
    cdef void expect(self, Py_ssize_t position, value):
        if self.last_position < position:
            self.last_position = position
            self.expected.clear()
        if self.last_position == position:
            self.expected.add(value)
    
    cdef ParseInfo unexpected(self):
        return ParseInfo(self.tokens, self.verbose, self.cache)


cdef class Parser:
    r'''Base class for determining whether and how to parse tokens in a token stream.
        This base class matches any of the listed alternatives.
    '''#"""#'''
    
    def __init__(self, *alternatives):
        states = []
        names = []
        for item in alternatives:
            if isinstance(item, tuple):
                parsers = [get_parser(value) for value in item]
                states.append(parsers)
                names.append('(' + ', '.join(parser.name for parser in parsers) + ')')
            elif isinstance(item, list):
                parsers = [get_parser(value) for value in item]
                states.append(parsers)
                states.append([])
                names.append('[' + ', '.join(parser.name for parser in parsers) + ']')
            else:
                parser = get_parser(item)
                states.append([parser])
                names.append(parser.name)
        if len(names) == 1:
            self.name = names[0]
        else:
            # This mimics the set notation, even when it's not appropriate.
            # For a more EBNF-compatible representation, consider:
            # self.name = '( ' + ' | '.join(names) + ' )'
            self.name = '{' + ', '.join(names) + '}'
        self.states = states
    
    def __repr__(self):
        return f"{self.__class__.__name__}({self.name})"
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        r'''Returns a Result based on whether this rule item matches the token stream.
        '''#"""#'''
        
        for state in self.states:
            yield from self.parse_sequence(state, position, info)

    def parse_sequence(self, sequence: list, position: Py_ssize_t, info: ParseInfo):
        head: Result
        tail: Result
        
        if not sequence:
            yield Result([], position)
            return
        
        if len(sequence) == 1:
            yield from sequence[0].parse(position, info)
            return
        
        rest = sequence[1:]
        for head in sequence[0].parse(position, info):
            for tail in self.parse_sequence(rest, head.position, info):
                yield Result(head.values + tail.values, tail.position)
    
    cdef set initial_tokens(self, bint recursive=False):
        result: set = set()
        for state in self.states:
            result.update(self.sequence_tokens(state, recursive))
        
        if True in result:
            # If everything is allowed, don't keep larger sets than necessary.
            return {True}
        
        return result
    
    cdef set sequence_tokens(self, list sequence, bint recursive=False):
        result: set = set()
        parser: Parser
        for parser in sequence:
            result.update(parser.initial_tokens(recursive))
            if None not in result:
                break
            result.discard(None)
        else:
            # Each item in the sequence, if any, is optional,
            # so the sequence as a whole can match without progressing.
            result.add(None)
        return result


cdef class Rule(Parser):
    cdef bint initialized
    cdef bint initializing
    cdef bint recursive
    cdef public bint cached
    cdef public bint single
    cdef public object result
    cdef public object translator
    cdef object function
    cdef dict initial
    
    def __init__(self, function):
        parser_cache[function] = self
        self.initialized = False
        self.initializing = False
        self.recursive = False
        self.initial = {}
        self.cached = True
        self.states = []
        self.name = function.__name__
        self.result = None
        self.single = False
        self.translator = None
        function(self)
        self.function = function
        if not self.states:
            from .exceptions import RuleError
            raise RuleError(f"Missing alternatives for rule function {self.name}")
        if self.single and self.result is not None:
            from .exceptions import RuleError
            raise RuleError(f"Rule function {self.name} is attempting to use both single and result")
    
    def __or__(self, value):
        if isinstance(value, tuple):
            self.states.append([get_parser(item) for item in value])
        else:
            self.states.append([get_parser(value)])
        return self
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        key: tuple
        result: Result
        processed: Result
        
        results = []
        if self.cached:
            key = (self, position)
            if key in info.cache:
                yield from info.cache[key]
                return
            
            # Make left-recursive rules slightly less wrong.
            info.cache[key] = results
        
        if not self.initialized:
            self.initial_tokens()
        
        states: set = set()
        token: Token = info.tokens[position]
        if token.__class__ in self.initial:
            states.update(self.initial[token.__class__])
        if token.number in self.initial:
            states.update(self.initial[token.number])
        if True in self.initial:
            states.update(self.initial[True])
        if None in self.initial:
            # Some states can return an empty result, along with non-empty results.
            # Since we don't know which ones come first, just process them together.
            states.update(self.initial[None])
        
        if not states:
            info.expect(position, self.name)
            return
        
        seen: set = set()
        for n in range(len(self.states)):
            if n not in states:
                continue
            
            for result in self.parse_sequence(self.states[n], position, info):
                if result.position in seen:
                    if info.verbose:
                        from .tokens import join_tokens
                        print(f'  {position}-{result.position}: Ambiguous {self.name}: {join_tokens(info.tokens[position:result.position])!r}')
                    continue
                seen.add(result.position)
                if info.verbose:
                    print(f'  {position}-{result.position}: {self.name} -> {[value for base, value in result.values]}')
                processed = Result(self.process(result.values), result.position)
                results.append(processed)
                yield processed
    
    cdef set initial_tokens(self, bint recursive=False):
        if self.initialized:
            return set(self.initial)
        
        if self.initializing:
            if self.recursive:
                # Just return what we have so far;
                # the parent rule knows not to save it permanently.
                return set(self.initial)
            
            # Left-recursion; collect as many states as possible,
            # so we can tell any intermediate steps about later states.
            self.recursive = True
            recursive = True
        
        self.initializing = True
        for n, state in enumerate(self.states):
            for value in self.sequence_tokens(state, recursive):
                if value in self.initial:
                    self.initial[value].add(n)
                else:
                    self.initial[value] = {n}
        
        if not recursive:
            self.initializing = False
            self.initialized = True
        
        return set(self.initial)
    
    def process(self, values):
        r'''Process a list of (base, value) pairs into a single (base, value) pair for the rule.
            For each pair, the base should either be a Rule function or a Token class.
            Uses the translator, result, and/or single fields set on the rule.
        '''#"""#'''
        
        if isinstance(self.result, dict):
            result = {
                name: self.collect_items(values, spec)
                for name, spec in self.result.items()
            }
        elif self.result is not None:
            result = self.collect_items(values, self.result)
        elif self.single:
            result = values[0][1] if values else None
        else:
            result = [value for base, value in values]
        
        if self.translator:
            result = Node(self.name, result, self.translator)
        
        return [(self.function, result)]
    
    def collect_items(self, values, spec):
        if isinstance(spec, set):
            sequence = spec
            single = True
        elif isinstance(spec, list):
            sequence = set(spec)
            single = False
        else:
            sequence = {spec}
            single = True
        
        results = []
        for base, item in values:
            if base in sequence:
                if single:
                    return item
                results.append(item)
        
        if single:
            return None
        
        return results


cdef class StringParser(Parser):
    r'''Matches any single token with the given string as its value.
    '''#"""#'''
    
    def __init__(self, value):
        parser_cache[value] = self
        self.value = value
        self.name = f"{value!r}"
        self.number = next(string_numbers)
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        token: Token
        token = info.tokens[position]
        if token.number == self.number:
            yield Result([(token.__class__, token)], position + 1)
        else:
            info.expect(position, self.name)
    
    cdef set initial_tokens(self, bint recursive=False):
        return {self.number}


cdef class TokenParser(Parser):
    r'''Matches any single token of the given Token subclass.
    '''#"""#'''
    
    cdef object token_class
    
    def __init__(self, token_class):
        parser_cache[token_class] = self
        self.token_class = token_class
        self.name = token_class.__name__
    
    def parse(self, position: Py_ssize_t, info: ParseInfo):
        token = info.tokens[position]
        if token.__class__ is self.token_class:
            yield Result([(token.__class__, token)], position + 1)
        else:
            info.expect(position, self.name)
    
    cdef set initial_tokens(self, bint recursive=False):
        return {self.token_class}


def get_parser(value):
    from .exceptions import RuleError
    from .tokens import Token
    
    try:
        return parser_cache[value]
    except (KeyError, TypeError):
        pass
    
    if isinstance(value, type) and issubclass(value, Token):
        result = TokenParser(value)
    elif isinstance(value, Parser):
        result = value
    elif callable(value):
        result = Rule(value)
    elif isinstance(value, str):
        result = StringParser(value)
    elif isinstance(value, (tuple, list)):
        result = Parser(value)
    elif isinstance(value, set):
        result = Parser(*value)
    else:
        raise RuleError(f"Unable to interpret {value.__class__.__name__} {value!r} as a rule item")
    
    return result
