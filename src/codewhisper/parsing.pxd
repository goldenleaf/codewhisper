cdef dict parser_cache


cdef class Result:
    cdef list values
    cdef Py_ssize_t position


cdef class ParseInfo:
    cdef list tokens
    cdef bint verbose
    cdef dict cache
    cdef Py_ssize_t last_position
    cdef set expected
    
    cdef void expect(self, Py_ssize_t position, value)
    cdef ParseInfo unexpected(self)


cdef class Parser:
    cdef readonly str name
    cdef readonly list states
    
    cdef set initial_tokens(self, bint recursive=*)
    cdef set sequence_tokens(self, list sequence, bint recursive=*)


cdef class StringParser(Parser):
    cdef Py_ssize_t number
    cdef str value
