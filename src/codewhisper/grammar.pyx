from .parsing cimport ParseInfo, Result, StringParser, parser_cache


class Grammar:
    def __init__(self, start, tokens, ignore=[]):
        from .parsing import get_parser
        self.start = get_parser(start)
        self.tokens = tokens
        self.ignore = set(ignore)
    
    def __repr__(self):
        tokens = ', '.join(cls.__name__ for cls in self.tokens)
        ignore = ", ignore=[', '.join(cls.__name__ for cls in self.ignore)]" if self.ignore else ""
        return f"{self.__class__.__name__}({self.start}, [{tokens}]{ignore})"
    
    def tokenize(self, text, verbose=False):
        from .exceptions import TokenizationError
        from .tokens import EOF
        
        parser: StringParser
        
        tokens = []
        line = 1
        character = 1
        position = 0
        length = len(text)
        ignored = []
        if verbose:
            print(f"Tokenizing {text!r}")
        while position < length:
            for token_class in self.tokens:
                tlength = token_class.match(text, position)
                if tlength is None:
                    continue
                value = text[position: position + tlength]
                if verbose:
                    print(f"Matched {tlength} characters with {token_class.__name__}: {value!r}")
                if token_class in self.ignore:
                    ignored.append(token_class(value, line, character, [], 0))
                else:
                    parser = parser_cache.get(value)
                    number = 0 if not parser else parser.number
                    tokens.append(token_class(value, line, character, ignored, number))
                    ignored = []
                if tlength <= 0:
                    continue
                position += tlength
                if "\n" in value:
                    line += value.count("\n")
                    character = tlength - value.rfind("\n")
                else:
                    character += tlength
                break
            else:
                snippet = text[position: position + 30]
                raise TokenizationError(text, line, character, snippet)
        tokens.append(EOF('', line, character, ignored, 0))
        return tokens
    
    def parse(self, text, verbose=False, **data):
        from .exceptions import ParseError
        
        result: Result
        
        if verbose:
            print(f"Parsing {text!r}")
        
        tokens = self.tokenize(text)
        length = len(tokens)
        final = length - 1
        info = ParseInfo(tokens, verbose, {})
        for result in self.start.parse(0, info):
            if final <= result.position <= length:
                (rule, nodes), = result.values
                return self.translate(nodes, verbose, **data)
        
        raise ParseError(text, tokens[info.last_position], info.expected)
    
    def translate(self, nodes, verbose=False, **data):
        return Scope(depth=0, verbose=verbose, **data).translate(nodes)


class Scope:
    def __init__(self, parent=None, **data):
        self.parent = parent
        self.data = data
    
    def __contains__(self, name):
        scope = self
        while scope is not None:
            if name in scope.data:
                return True
            scope = scope.parent
        return False
    
    def __getitem__(self, name):
        scope = self
        while scope is not None:
            if name in scope.data:
                return scope.data[name]
            scope = scope.parent
        raise KeyError(name)
    
    def __setitem__(self, name, value):
        self.data[name] = value
    
    def get(self, name, default=None):
        scope = self
        while scope is not None:
            if name in scope.data:
                return scope.data[name]
            scope = scope.parent
        return default
    
    def subscope(self, **data):
        return self.__class__(self, **data)
    
    def translate(self, value, **data):
        if self['verbose']:
            depth = self['depth']
            data['depth'] = depth + 1
            print(f"{' '*depth}Translating {value!r}")
        scope = self.subscope(**data) if data else self
        if isinstance(value, list):
            return [scope.translate(item) for item in value]
        if isinstance(value, dict):
            return {key: scope.translate(item) for key, item in value.items()}
        translator = getattr(value, "translate", None)
        if translator is None:
            return value
        return translator(scope)
