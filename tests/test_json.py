from json import dumps, loads
from math import isnan, nan
from string import printable
from unittest import TestCase

from hypothesis import given
from hypothesis.strategies import booleans, dictionaries, floats, lists, none, recursive, text

from examples import json_patterns, json_rules


trees = recursive(
    none() | booleans() | floats(allow_nan=False) | text(),
    (lambda children: lists(children) | dictionaries(text(printable), children)),
    max_leaves = 90,
)

class JsonTests(TestCase):
    def parse(self, value):
        return loads(value)
    
    @given(value=trees)
    def test_json(self, value):
        # Structures created by Python's json package should be parsed correctly.
        # Note that NaN is explicitly excluded because it never equals itself.
        result = self.parse(dumps(value))
        self.assertEqual(result, value)
    
    @given(value=text())
    def test_strings(self, value):
        # Arbitrary strings should either parse like json.loads,
        # or throw ValueError like it does.
        try:
            expected = loads(value)
        except ValueError:
            with self.assertRaises(ValueError):
                self.parse(value)
        else:
            result = self.parse(value)
            self.assertEqual(result, expected)
    
    def test_nan(self):
        with self.subTest(value=nan):
            result = self.parse(dumps(nan))
            self.assertTrue(isnan(result))
        
        value = [nan]
        with self.subTest(value=value):
            result = self.parse(dumps(value))
            self.assertIsInstance(result, list)
            self.assertEqual(len(result), 1)
            self.assertTrue(isnan(result[0]))
        
        value = {"nan": nan}
        with self.subTest(value=value):
            result = self.parse(dumps(value))
            self.assertIsInstance(result, dict)
            self.assertEqual(len(result), 1)
            self.assertCountEqual(result, ["nan"])
            self.assertTrue(isnan(result["nan"]))


class JsonPatternsTests(JsonTests):
    parse = json_patterns.grammar.parse


class JsonRulesTests(JsonTests):
    parse = json_rules.grammar.parse
