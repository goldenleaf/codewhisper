from math import isnan
from operator import add, mul, sub, truediv
from unittest import TestCase

from hypothesis import example, given
from hypothesis.strategies import booleans, composite, floats, integers, lists, recursive, sampled_from

from examples.calc import grammar


@composite
def numbers(draw):
    value = draw(floats() | integers())
    string = str(value)
    value = float(value)
    if string.startswith("-"):
        prefix = ""
    else:
        prefix = draw(sampled_from(["", "-", "- "]))
        if prefix:
            value = -value
    if string[-1].isalpha():
        suffix = ""
    else:
        suffix = draw(sampled_from(["", "i", " i", "j", " j"]))
        if suffix:
            value *= 1j
    return (prefix + string + suffix, value)

@composite
def wrapped_expressions(draw, children=numbers()):
    if draw(booleans()):
        main_op = add
        second_op = sub
        main = ['+', ' + ']
        secondary = ['-', ' - ']
    else:
        main_op = mul
        second_op = truediv
        main = ['*', ' * ', '×', ' × ']
        secondary = ['/', ' / ', '÷', ' ÷ ']
    
    string, value = draw(children)
    for expr, val in draw(lists(children, max_size=5)):
        value = main_op(value, val)
        string += draw(sampled_from(main))
        string += expr
    
    if draw(booleans()):
        expr, val = draw(children)
        try:
            value = second_op(value, val)
        except ZeroDivisionError:
            value = main_op(value, val)
            string += draw(sampled_from(main))
        else:
            string += draw(sampled_from(secondary))
        string += expr
    
    string = draw(sampled_from([
        f"({string})",
        f"( {string} )",
        f"-({string})",
        f"- ({string})",
        f"-({string})",
        f"-( {string} )",
    ]))
    
    if string.startswith("-"):
        value = -value
    return string, value


expressions = recursive(numbers(), wrapped_expressions)

class CalcTests(TestCase):
    @given(expression=expressions)
    @example(expression=("1+2*3", 7))
    def test_calculator(self, expression):
        string, value = expression
        result = grammar.parse(string)
        if isinstance(value, float) and isnan(value):
            self.assertIsInstance(result, float)
            self.assertTrue(isnan(result))
        elif isinstance(value, complex) and (isnan(value.real) or isnan(value.imag)):
            self.assertIsInstance(result, complex)
            if isnan(value.real):
                self.assertTrue(isnan(result.real))
            else:
                self.assertEqual(result.real, value.real)
            if isnan(value.imag):
                self.assertTrue(isnan(result.imag))
            else:
                self.assertEqual(result.imag, value.imag)
        else:
            self.assertEqual(result, value)
