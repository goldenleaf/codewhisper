import csv

from io import StringIO
from unittest import TestCase

from hypothesis import assume, given
from hypothesis.strategies import characters, composite, lists, text

from examples.csv import csv_lines, csv_records


def raw_value():
    # Cell values to be written by csv.writer; may contain commas, quotation marks, and newlines,
    # but may not contain null characters as of Python 3.10, due to needing an escape character.
    return text(characters(blacklist_categories=('Cs',), blacklist_characters='\x00'))


def simple_value():
    # Cell values to be concatenated with commas.
    # May not contain commas, quotation marks, or newlines.
    return text(characters(blacklist_categories=('Cs',), blacklist_characters='\r\n,"'), min_size=1)


@composite
def multiple_dicts(draw, keys=lists(raw_value(), min_size=1), values=raw_value()):
    headers = draw(keys)
    size = len(headers)
    assume(len(set(headers)) == size)
    values = draw(lists(lists(raw_value(), min_size=size, max_size=size)))
    rows = [{k: v for k, v in zip(headers, vals)} for vals in values]
    return headers, rows


class CsvTests(TestCase):
    @given(value=lists(lists(raw_value(), min_size=1)))
    def test_lists(self, value):
        # Text emitted by Python's csv.writer should be parsed correctly.
        output = StringIO()
        writer = csv.writer(output)
        writer.writerows(value)
        output.seek(0)
        result = csv_lines.parse(output.read())
        self.assertEqual(result, value)
    
    @given(value=multiple_dicts())
    def test_records(self, value):
        # Text emitted by Python's csv.DictWriter should be parsed correctly.
        keys, rows = value
        output = StringIO()
        writer = csv.DictWriter(output, keys)
        writer.writeheader()
        writer.writerows(rows)
        output.seek(0)
        result = csv_records.parse(output.read())
        self.assertEqual(result, rows)
    
    @given(value=lists(lists(simple_value(), min_size=1), min_size=1))
    def test_final_line_ending(self, value):
        # The final line in a CSV may or may not have a line ending.
        text = "\r\n".join(",".join(row) for row in value)
        result = csv_lines.parse(text)
        self.assertEqual(result, value)
        
        result = csv_lines.parse(text + "\r\n")
        self.assertEqual(result, value)
