from datetime import date, datetime, time, timedelta, timezone
from textwrap import dedent
from unittest import TestCase

from codewhisper.exceptions import InputError

from examples.toml import TomlException, toml_grammar


class TomlTests(TestCase):
    def test_comments(self):
        # A hash symbol marks the rest of the line as a comment, except when inside a string.
        result = toml_grammar.parse(dedent("""
            # This is a full-line comment
            key = "value"  # This is a comment at the end of a line
            another = "# This is not a comment"
        """))
        
        self.assertEqual(result, {
            'key': 'value',
            'another': '# This is not a comment',
        })
    
    def test_control_characters_in_comments(self):
        # Control characters other than tab (U+0000 to U+0008, U+000A to U+001F, U+007F) are not permitted in comments.
        template = dedent("""
            key = "value" # Control character: "{character}"
        """)
        
        for c in range(0x00, 0x20):
            with self.subTest(c=hex(c)):
                document = template.format(character=chr(c))
                if c == 0x09:
                    toml_grammar.parse(document)
                else:
                    with self.assertRaises(InputError):
                        toml_grammar.parse(document)
        
        with self.subTest(c="0x7F"):
            with self.assertRaises(InputError):
                toml_grammar.parse(template.format(character="\x7F"))
    
    def test_unspecified_value(self):
        # Unspecified values are invalid.
        with self.assertRaises(InputError):
            toml_grammar.parse(dedent("""
                key = # INVALID
            """))
        
        with self.assertRaises(InputError):
            toml_grammar.parse("key =")
    
    def test_missing_newline(self):
        # There must be a newline (or EOF) after a key/value pair.
        with self.assertRaises(InputError):
            toml_grammar.parse(dedent("""
                first = "Tom" last = "Preston-Werner"
            """))
    
    def test_bare_keys(self):
        # Bare keys may only contain ASCII letters, ASCII digits, underscores, and dashes.
        result = toml_grammar.parse(dedent("""
            key = "value"
            bare_key = "value"
            bare-key = "value"
            1234 = "value"
        """))
        
        self.assertEqual(result, {
            "key": "value",
            "bare_key": "value",
            "bare-key": "value",
            "1234": "value",
        })
    
    def test_quoted_keys(self):
        # Quoted keys follow the exact same rules as either basic strings or literal strings.
        result = toml_grammar.parse(dedent("""
            "127.0.0.1" = "value"
            "character encoding" = "value"
            "ʎǝʞ" = "value"
            'key2' = "value"
            'quoted "value"' = "value"
        """))
        
        self.assertEqual(result, {
            "127.0.0.1": "value",
            "character encoding": "value",
            "ʎǝʞ": "value",
            "key2": "value",
            'quoted "value"': "value",
        })
    
    def test_empty_keys(self):
        # A bare key must be non-empty, but an empty quoted key is allowed (though discouraged).
        with self.assertRaises(InputError):
            toml_grammar.parse('= "no key name"')
        
        self.assertEqual(toml_grammar.parse('"" = "blank"'), {"": "blank"})
        self.assertEqual(toml_grammar.parse("'' = 'blank'"), {"": "blank"})
    
    def test_dotted_keys(self):
        # Dotted keys are a sequence of bare or quoted keys joined with a dot.
        result = toml_grammar.parse(dedent("""
            name = "Orange"
            physical.color = "orange"
            physical.shape = "round"
            site."google.com" = true
        """))
        
        self.assertEqual(result, {
            "name": "Orange",
            "physical": {
                "color": "orange",
                "shape": "round",
            },
            "site": {
                "google.com": True,
            },
        })
    
    def test_dotted_key_whitespace(self):
        # Whitespace around dot-separated parts is ignored.
        result = toml_grammar.parse(dedent("""
            fruit.name = "banana"     # this is best practice
            fruit. color = "yellow"    # same as fruit.color
            fruit . flavor = "banana"   # same as fruit.flavor
        """))
        
        self.assertEqual(result, {
            "fruit": {
                "name": "banana",
                "color": "yellow",
                "flavor": "banana",
            },
        })
    
    def test_duplicate_keys(self):
        # Defining a key multiple times is invalid.
        # Todo: Use a better exception.
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                name = "Tom"
                name = "Pradyun"
            """))
        
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                spelling = "favorite"
                "spelling" = "favourite"
            """))
    
    def test_defined_tables(self):
        # As long as a key hasn't been directly defined, you may still write to it and to names within it.
        result = toml_grammar.parse(dedent("""
            fruit.apple.smooth = true
            fruit.orange = 2
        """))
        
        self.assertEqual(result, {
            "fruit": {
                "apple": {"smooth": True},
                "orange": 2,
            },
        })
        
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                fruit.apple = 1
                fruit.apple.smooth = true
            """))
    
    def test_dotted_float_keys(self):
        result = toml_grammar.parse(dedent("""
            3.14159 = "pi"
        """))
        
        self.assertEqual(result, {"3": {"14159": "pi"}})
    
    def test_tables(self):
        # Tables are defined by headers, with square brackets on a line by themselves.
        result = toml_grammar.parse(dedent("""
            [table]
        """))
        self.assertEqual(result, {"table": {}})
        
        result = toml_grammar.parse(dedent("""
            [table-1]
            key1 = "some string"
            key2 = 123
            
            [table-2]
            key1 = "another string"
            key2 = 456
        """))
        self.assertEqual(result, {
            "table-1": {"key1": "some string", "key2": 123},
            "table-2": {"key1": "another string", "key2": 456},
        })
        
        result = toml_grammar.parse(dedent("""
            [dog."tater.man"]
            type.name = "pug"
        """))
        self.assertEqual(result, {"dog": {"tater.man": {"type": {"name": "pug"}}}})
        
        result = toml_grammar.parse(dedent("""
            [a.b.c]            # this is best practice
            [ d.e.f ]          # same as [d.e.f]
            [ g .  h  . i ]    # same as [g.h.i]
            [ j . "ʞ" . 'l' ]  # same as [j."ʞ".'l']
        """))
        self.assertEqual(result, {
            "a": {"b": {"c": {}}},
            "d": {"e": {"f": {}}},
            "g": {"h": {"i": {}}},
            "j": {"ʞ": {"l": {}}},
        })
        
        result = toml_grammar.parse(dedent("""
            # [x] you
            # [x.y] don't
            # [x.y.z] need these
            [x.y.z.w] # for this to work
            
            [x] # defining a super-table afterward is ok
        """))
        self.assertEqual(result, {
            "x": {"y": {"z": {"w": {}}}},
        })
        
        result = toml_grammar.parse(dedent("""
            # VALID BUT DISCOURAGED
            [fruit.apple]
            [animal]
            [fruit.orange]
        """))
        self.assertEqual(result, {
            "animal": {},
            "fruit": {
                "apple": {},
                "orange": {},
            },
        })
        
        result = toml_grammar.parse(dedent("""
            # Top-level table begins.
            name = "Fido"
            breed = "pug"
            
            # Top-level table ends.
            [owner]
            name = "Regina Dogman"
            member_since = 1999-08-04
        """))
        self.assertEqual(result, {
            "name": "Fido",
            "breed": "pug",
            "owner": {
                "name": "Regina Dogman",
                "member_since": date(1999, 8, 4),
            },
        })
    
    def test_table_redefinition(self):
        # Dotted keys create and define a table for each key part before the last one,
        # provided that such tables were not previously created.
        
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                # DO NOT DO THIS
                
                [fruit]
                apple = "red"
                
                [fruit]
                orange = "orange"
            """))
        
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                # DO NOT DO THIS EITHER
                
                [fruit]
                apple = "red"
                
                [fruit.apple]
                texture = "smooth"
            """))
        
        result = toml_grammar.parse(dedent("""
            fruit.apple.color = "red"
            # Defines a table named fruit
            # Defines a table named fruit.apple
            
            fruit.apple.taste.sweet = true
            # Defines a table named fruit.apple.taste
            # fruit and fruit.apple were already created
        """))
        self.assertEqual(result, {
            "fruit": {"apple": {
                "color": "red",
                "taste": {"sweet": True},
            }},
        })
        
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                [fruit]
                apple.color = "red"
                apple.taste.sweet = true
                
                [fruit.apple]  # INVALID
            """))
        
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                [fruit]
                apple.color = "red"
                apple.taste.sweet = true
                
                [fruit.apple.taste]
            """))
        
        result = toml_grammar.parse(dedent("""
            [fruit]
            apple.color = "red"
            apple.taste.sweet = true
            
            [fruit.apple.texture]  # you can add sub-tables
            smooth = true
        """))
        self.assertEqual(result, {
            "fruit": {"apple": {
                "color": "red",
                "taste": {"sweet": True},
                "texture": {"smooth": True},
            }},
        })
    
    def test_table_array(self):
        # Arrays of tables can be expressed by using a header with a name in double brackets.
        result = toml_grammar.parse(dedent("""
            [[products]]
            name = "Hammer"
            sku = 738594937

            [[products]]  # empty table within the array

            [[products]]
            name = "Nail"
            sku = 284758393

            color = "gray"
        """))
        self.assertEqual(result, {"products": [
            {"name": "Hammer", "sku": 738594937},
            {},
            {"name": "Nail", "sku": 284758393, "color": "gray"}
        ]})
        
        # Attempting to append to a statically defined array, even if that array is empty, must produce an error at parse time.
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                fruits = []
                
                [[fruits]] # Not allowed
            """))
        
        # Attempting to define a normal table with the same name as an already established array
        # must produce an error at parse time.
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                [[fruits]]
                
                [fruits]
            """))
        
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                [[fruits]]
                name = "apple"

                [[fruits.varieties]]
                name = "red delicious"

                # INVALID: This table conflicts with the previous array of tables
                [fruits.varieties]
                name = "granny smith"
            """))
        
        # Attempting to redefine a normal table as an array must likewise produce an error.
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                [[fruits]]
                name = "apple"
                
                [[fruits.varieties]]
                name = "red delicious"
                
                [fruits.physical]
                color = "red"
                shape = "round"
                
                # INVALID: This array of tables conflicts with the previous table
                [[fruits.physical]]
                color = "green"
            """))
    
    def test_array_references(self):
        # Any reference to an array of tables points to the most recently defined table element of the array.
        result = toml_grammar.parse(dedent("""
            [[fruits]]
            name = "apple"

            [fruits.physical]  # subtable
            color = "red"
            shape = "round"

            [[fruits.varieties]]  # nested array of tables
            name = "red delicious"

            [[fruits.varieties]]
            name = "granny smith"


            [[fruits]]
            name = "banana"

            [[fruits.varieties]]
            name = "plantain"
        """))
        self.assertEqual(result, {
            "fruits": [
                {
                    "name": "apple",
                    "physical": {"color": "red", "shape": "round"},
                    "varieties": [{"name": "red delicious"}, {"name": "granny smith"}],
                },
                {
                    "name": "banana",
                    "varieties": [{"name": "plantain"}],
                },
            ]
        })
        
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent("""
                [fruit.physical]  # subtable, but to which parent element should it belong?
                color = "red"
                shape = "round"
                
                [[fruit]]  # parser must throw an error upon discovering that "fruit" is
                           # an array rather than a table
                name = "apple"
            """))


class TomlValueTests(TestCase):
    def assertParses(self, value, expected):
        value = value.strip()
        with self.subTest(value=value):
            result = toml_grammar.parse(f"key = {value}")
            self.assertEqual(result, {"key": expected})
    
    def assertInvalid(self, value):
        with self.subTest(value=value):
            with self.assertRaises(InputError):
                toml_grammar.parse(f"key = {value}")
    
    def test_basic_strings(self):
        # Basic strings are surrounded by quotation marks.
        self.assertParses(r'''
            "I'm a string. \"You can quote me\". Name\tJos\u00E9\nLocation\tSF."
        ''', "I'm a string. \"You can quote me\". Name\tJosé\nLocation\tSF.")
    
    def test_escape_sequences(self):
        # For convenience, some popular characters have a compact escape sequence.
        escapes = {
            'b': 0x0008,
            't': 0x0009,
            'n': 0x000A,
            'f': 0x000C,
            'r': 0x000D,
            '"': 0x0022,
            '\\': 0x005C,
            'u1234': 0x1234,
            'U00012345': 0x00012345,
        }
        
        for c in escapes:
            self.assertParses(f'"\\{c}"', chr(escapes[c]))
    
    def test_multiline_basic_strings(self):
        # Multi-line basic strings are surrounded by three quotation marks on each side and allow newlines.
        result = toml_grammar.parse(dedent('''
            str1 = """
            Roses are red
            Violets are blue"""
        '''))
        
        self.assertIn(result["str1"], [
            "Roses are red\nViolets are blue",
            "Roses are red\r\nViolets are blue",
        ])
    
    def test_line_ending_backslash(self):
        # When the last non-whitespace character on a line is an unescaped backslash,
        # it will be trimmed along with all whitespace (including newlines)
        # up to the next non-whitespace character or closing delimiter.
        result = toml_grammar.parse(dedent(r'''
            str1 = "The quick brown fox jumps over the lazy dog."

            str2 = """
            The quick brown \


              fox jumps over \
                the lazy dog."""

            str3 = """\
                   The quick brown \
                   fox jumps over \
                   the lazy dog.\
                   """
        '''))
        sentence = "The quick brown fox jumps over the lazy dog."
        self.assertEqual(result, {
            'str1': sentence,
            'str2': sentence,
            'str3': sentence,
        })
    
    def test_quotation_marks_in_multiline_strings(self):
        # You can write a quotation mark, or two adjacent quotation marks, anywhere inside a multi-line basic string.
        self.assertParses(r'''
            """Here are two quotation marks: "". Simple enough."""
        ''', 'Here are two quotation marks: "". Simple enough.')
        
        self.assertParses(r'''
            """Here are three quotation marks: ""\"."""
        ''', 'Here are three quotation marks: """.')
        
        self.assertParses(r'''
            """Here are fifteen quotation marks: ""\"""\"""\"""\"""\"."""
        ''', 'Here are fifteen quotation marks: """"""""""""""".')
        
        self.assertParses(r'''
            """"This," she said, "is just a pointless statement.""""
        ''', '"This," she said, "is just a pointless statement."')
        
        self.assertInvalid(r'''"""Here are three quotation marks: """."""''')
    
    def test_literal_strings(self):
        # Literal strings are surrounded by single quotes.
        self.assertParses(r'''
            'C:\Users\nodejs\templates'
        ''', r'C:\Users\nodejs\templates')
        
        self.assertParses(r'''
            '\\ServerX\admin$\system32\'
        ''', '\\\\ServerX\\admin$\\system32\\')
        
        self.assertParses(r'''
            'Tom "Dubs" Preston-Werner'
        ''', 'Tom "Dubs" Preston-Werner')
        
        self.assertParses(r'''
            '<\i\c*\s*>'
        ''', r'<\i\c*\s*>')
    
    def test_multiline_literal_strings(self):
        # Multi-line literal strings are surrounded by three single quotes on each side and allow newlines.
        self.assertParses(r"""
            '''I [dw]on't need \d{2} apples'''
        """, r"I [dw]on't need \d{2} apples")
        
        # A newline immediately following the opening delimiter will be trimmed.
        self.assertParses(dedent(r"""
            '''
            The first newline is
            trimmed in raw strings.
               All other whitespace
               is preserved.
            '''
        """), '\n'.join([
            "The first newline is",
            "trimmed in raw strings.",
            "   All other whitespace",
            "   is preserved.",
            "",
        ]))
    
    def test_apostrophes_in_multiline_literals(self):
        # You can write 1 or 2 single quotes anywhere within a multi-line literal string,
        # but sequences of three or more single quotes are not permitted.
        
        text = 'Here are fifteen quotation marks: """""""""""""""'
        result = toml_grammar.parse(f"quot15 = '''{text}'''")
        self.assertEqual(result, {'quot15': text})
        
        text = "Here are fifteen apostrophes: '''''''''''''''"
        with self.assertRaises(InputError):
            toml_grammar.parse(f"apos15 = '''{text}'''")
        result = toml_grammar.parse(f'apos15 = "{text}"')
        self.assertEqual(result, {'apos15': text})
        
        text = "'That,' she said, 'is still pointless.'"
        result = toml_grammar.parse(f"str = '''{text}'''")
        self.assertEqual(result, {'str': text})
    
    def test_integer(self):
        # Integers are whole numbers.
        self.assertParses("+99", 99)
        self.assertParses("42", 42)
        self.assertParses("0", 0)
        self.assertParses("-17", -17)
        
        # Leading zeros are not allowed.
        self.assertInvalid("0123")
        
        # Integer values -0 and +0 are valid and identical to an unprefixed zero.
        self.assertParses("-0", 0)
        self.assertParses("+0", 0)
    
    def test_integer_underscores(self):
        # For large numbers, you may use underscores between digits to enhance readability.
        self.assertParses("1_000", 1000)
        self.assertParses("5_349_221", 5349221)
        self.assertParses("53_49_221", 5349221)
        self.assertParses("1_2_3_4_5", 12345)
        
        # Each underscore must be surrounded by at least one digit on each side.
        self.assertInvalid("1__000")
        self.assertInvalid("_100")
        self.assertInvalid("100_")
    
    def test_integer_formats(self):
        # Non-negative integer values may also be expressed in hexadecimal, octal, or binary.
        self.assertParses("0xDEADBEEF", 0xDEADBEEF)
        self.assertParses("0xdeadbeef", 0xDEADBEEF)
        self.assertParses("0xdead_beef", 0xDEADBEEF)
        
        self.assertParses("0o01234567", 0o1234567)
        self.assertParses("0o755", 0o755)
        
        self.assertParses("0b11010110", 0b11010110)
        
        # In these formats, leading + is not allowed.
        self.assertInvalid("0x+DEADBEEF")
        self.assertInvalid("+0xDEADBEEF")
    
    def test_float(self):
        # Floats should be implemented as IEEE 754 binary64 values.
        
        # With a fractional part
        self.assertParses("+1.0", 1.0)
        self.assertParses("3.1415", 3.1415)
        self.assertParses("-0.01", -0.01)
        
        # With an exponent
        self.assertParses("5e+22", 5e22)
        self.assertParses("1e06", 1e6)
        self.assertParses("-2E-2", -2E-2)
        
        # Both
        self.assertParses("6.626e-34", 6.626e-34)
        
        # The fractional part must precede the exponent part.
        self.assertInvalid("6e-34.626")
        
        # Underscores
        self.assertParses("224_617.445_991_228", 224617.445991228)
    
    def test_special_float_values(self):
        # Special float values can also be expressed.
        from math import inf, isnan
        
        self.assertParses("+0.0", +0.0)
        self.assertParses("-0.0", -0.0)
        
        self.assertParses("inf", inf)
        self.assertParses("+inf", inf)
        self.assertParses("-inf", -inf)
        
        # NaN doesn't compare equal to itself.
        for value in ["nan", "+nan", "-nan"]:
            with self.subTest(value=value):
                result = toml_grammar.parse(f"key = {value}")
                self.assertTrue(isnan(result.get("key")))
        
        # They are always lowercase.
        self.assertInvalid("Inf")
        self.assertInvalid("INF")
        self.assertInvalid("-Inf")
        self.assertInvalid("-INF")
        self.assertInvalid("Nan")
        self.assertInvalid("NaN")
        self.assertInvalid("NAN")
        self.assertInvalid("-Nan")
        self.assertInvalid("-NaN")
        self.assertInvalid("-NAN")
    
    def test_boolean(self):
        # Booleans are just the tokens you're used to. Always lowercase.
        self.assertParses("true", True)
        self.assertParses("false", False)
        
        self.assertInvalid("True")
        self.assertInvalid("TRUE")
        self.assertInvalid("False")
        self.assertInvalid("FALSE")
    
    def test_datetime(self):
        # To unambiguously represent a specific instant in time, you may use an RFC 3339 formatted
        # date-time with offset.
        self.assertParses("1979-05-27 07:32:00Z", datetime(1979, 5, 27, 7, 32, 0, tzinfo=timezone.utc))
        self.assertParses("1979-05-27T07:32:00Z", datetime(1979, 5, 27, 7, 32, 0, tzinfo=timezone.utc))
        self.assertParses(
            "1979-05-27T00:32:00-07:00",
            datetime(1979, 5, 27, 0, 32, 0, tzinfo=timezone(timedelta(hours=-7))),
        )
        self.assertParses(
            "1979-05-27T00:32:00.999999-07:00",
            datetime(1979, 5, 27, 0, 32, 0, 999999, tzinfo=timezone(timedelta(hours=-7))),
        )
    
    def test_local_datetime(self):
        # If you omit the offset from an RFC 3339 formatted date-time,
        # it will represent the given date-time without any relation to an offset or timezone.
        self.assertParses("1979-05-27T07:32:00", datetime(1979, 5, 27, 7, 32, 0))
        self.assertParses("1979-05-27T00:32:00.999999", datetime(1979, 5, 27, 0, 32, 0, 999999))
    
    def test_local_date(self):
        # If you include only the date portion of an RFC 3339 formatted date-time,
        # it will represent that entire day without any relation to an offset or timezone.
        self.assertParses("1979-05-27", date(1979, 5, 27))
    
    def test_local_time(self):
        # If you include only the time portion of an RFC 3339 formatted date-time, it will
        # represent that time of day without any relation to a specific day or any offset or timezone.
        self.assertParses("07:32:00", time(7, 32, 0))
        self.assertParses("00:32:00.999999", time(0, 32, 0, 999999))
    
    def test_arrays(self):
        # Arrays are square brackets with values inside. Elements are separated by commas.
        self.assertParses("[ 1, 2, 3 ]", [1, 2, 3])
        self.assertParses('[ "red", "yellow", "green" ]', ["red", "yellow", "green"])
        self.assertParses("[ [ 1, 2 ], [3, 4, 5] ]", [[1, 2], [3, 4, 5]])
        self.assertParses('[ [ 1, 2 ], ["a", "b", "c"] ]', [[1, 2], ["a", "b", "c"]])
        self.assertParses(r'''["all strings", 'are the same', """type"""]''', ["all strings", "are the same", "type"])
        
        # Values of different types may be mixed.
        self.assertParses('[ 0.1, 0.2, 0.5, 1, 2, 5 ]', [0.1, 0.2, 0.5, 1, 2, 5])
        self.assertParses(dedent(r'''
            [
                "Foo Bar <foo@example.com>",
                { name = "Baz Qux", email = "bazqux@example.com", url = "https://example.com/bazqux" }
            ]
        '''), [
            "Foo Bar <foo@example.com>",
            {"name": "Baz Qux", "email": "bazqux@example.com", "url": "https://example.com/bazqux"},
        ])
        
        # Trailing commas and comments are allowed.
        self.assertParses(dedent(r'''
            [
                1, 2, 3
            ]
        '''), [1, 2, 3])
        self.assertParses(dedent(r'''
            [
                1,
                2, # this is ok
            ]
        '''), [1, 2])
        
        # An array may be empty.
        self.assertParses("[]", [])
    
    def test_inline_tables(self):
        # Inline tables provide a more compact syntax for expressing tables.
        self.assertParses('{ first = "Tom", last = "Preston-Werner" }', {'first': "Tom", 'last': "Preston-Werner"})
        self.assertParses('{ x = 1, y = 2 }', {'x': 1, 'y': 2})
        self.assertParses('{ type.name = "pug" }', {'type': {'name': 'pug'}})
        
        # A trailing comma is not permitted after the last key/value pair in an inline table.
        self.assertInvalid('{ x = 1, y = 2, }')
        
        # No newlines are allowed between the curly braces unless they are valid within a value.
        self.assertInvalid(dedent(r'''
            { x = 1,
            y = 2}
        '''))
        self.assertInvalid(dedent(r'''
            {
            x = 1,
            y = 2
            }
        '''))
        self.assertInvalid(dedent(r'''
            {
                x = 1,
                y = 2,
            }
        '''))
        
        # Keys and sub-tables cannot be added outside the braces.
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent(r'''
                [product]
                type = { name = "Nail" }
                type.edible = false
            '''))
        
        # Similarly, inline tables cannot be used to add keys or sub-tables to an already-defined table.
        with self.assertRaises(TomlException):
            toml_grammar.parse(dedent(r'''
                [product]
                type.name = "Nail"
                type = { edible = false }
            '''))
        
        # Inline tables may be nested in arrays.
        self.assertParses(dedent(r'''
            [ { x = 1, y = 2, z = 3 },
              { x = 7, y = 8, z = 9 },
              { x = 2, y = 4, z = 8 } ]
        '''), [
            {'x': 1, 'y': 2, 'z': 3},
            {'x': 7, 'y': 8, 'z': 9},
            {'x': 2, 'y': 4, 'z': 8},
        ])
