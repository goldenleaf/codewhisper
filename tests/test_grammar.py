from unittest import TestCase

from hypothesis import assume, given
from hypothesis.strategies import characters, dictionaries, integers, lists, text

from codewhisper.grammar import Grammar
from codewhisper.special import Commas
from codewhisper.tokens import CHARACTER, CharacterToken, StringToken


def line_text():
    return text(characters(blacklist_categories=('Cs', 'Cc', 'Zs')), min_size=1)


class TokenizationTests(TestCase):
    @given(values=lists(line_text(), min_size=1))
    def test_start_position(self, values):
        # Each token should have a 1-based line and character number for its position in the input string.
        class TOKEN(StringToken):
            strings = values
        
        class WHITESPACE(CharacterToken):
            characters = " \n"
        
        def start(rule):
            rule | Commas(TOKEN, separator=WHITESPACE)
        
        grammar = Grammar(start=start, tokens=[TOKEN, WHITESPACE])
        
        # All tokens on the same line
        position = 1
        for token in grammar.tokenize(" ".join(values)):
            self.assertEqual(token.line, 1)
            self.assertEqual(token.character, position)
            position += len(token.value)
        
        # Each token on a separate line
        line = 1
        position = 1
        for token in grammar.tokenize("\n".join(values)):
            self.assertEqual(token.line, line)
            self.assertEqual(token.character, position)
            if isinstance(token, WHITESPACE):
                line += 1
                position = 1
            else:
                position += len(token.value)
    
    @given(value=line_text(), lines=integers(min_value=1, max_value=5))
    def test_embedded_newlines(self, value, lines):
        # Line and character numbers should include all newlines inside a token.
        multiline = "\n".join([value] * lines)
        class TOKEN(StringToken):
            strings = {multiline, value}
        
        def start(rule):
            rule | (TOKEN, TOKEN)
            rule.translator = lambda node, scope: node
        
        grammar = Grammar(start=start, tokens=[TOKEN])
        tokens = grammar.parse(multiline + value)
        positions = [(token.line, token.character) for token in tokens]
        self.assertEqual(positions, [(1, 1), (lines, len(value) + 1)])


class ScopeTests(TestCase):
    @given(pattern=characters(), data=dictionaries(text(), text() | integers()))
    def test_initial_data(self, pattern, data):
        # Keyword parameters passed to Grammar.parse() will be passed to the Scope.
        def start(rule):
            rule | CHARACTER
            rule.result = {'text': [CHARACTER]}
            rule.translator = lambda node, scope: {
                'text': scope.translate(node['text']),
                'data': {key: scope[key] for key in data},
            }
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(pattern, **data)
        self.assertEqual(result, {
            'text': [pattern],
            'data': data,
        })
    
    @given(pattern=characters(), name=text(), first=text(), second=text())
    def test_subscope(self, pattern, name, first, second):
        # Subscopes can read data from the parent, but don't write to it.
        assume(first != second)
        
        def start(rule):
            rule | CHARACTER
            def translator(node, scope):
                data = {}
                scope[name] = first
                subscope = scope.subscope()
                data['before'] = subscope[name]
                subscope[name] = second
                data['after'] = subscope[name]
                data['parent'] = scope[name]
                return data
            rule.translator = translator
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(pattern)
        self.assertEqual(result, {
            'before': first,
            'after': second,
            'parent': first,
        })
    
    @given(pattern=characters(), name=text(), value=text(), default=text())
    def test_subscope_parameters(self, pattern, name, value, default):
        # Parameters can be passed to subscope() to initialize the new scope.
        def start(rule):
            rule | CHARACTER
            def translator(node, scope):
                subscope = scope.subscope(**{name: value})
                return {
                    'parent': scope.get(name, default),
                    'child': subscope.get(name, default),
                }
            rule.translator = translator
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(pattern)
        self.assertEqual(result, {
            'parent': default,
            'child': value,
        })
    
    @given(pattern=characters(), name=text(), first=text(), second=text())
    def test_implicit_subscope(self, pattern, name, first, second):
        # Parameters can be passed to scope.translate() to initialize a new subscope.
        def start(rule):
            rule | subrule
            def translator(node, scope):
                scope[name] = first
                result = scope.translate(node, **{name: second})
                return {
                    'result': result,
                    'parent': scope[name],
                }
            rule.translator = translator
        
        def subrule(rule):
            rule | CHARACTER
            def translator(node, scope):
                return {'child': scope[name]}
            rule.translator = translator
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(pattern)
        self.assertEqual(result, {
            'parent': first,
            'result': [{'child': second}],
        })
    
    @given(pattern=characters(), name=text(), first=text(), second=text())
    def test_mutable_data(self, pattern, name, first, second):
        # Mutable items in a scope are shared with subscopes unless they assign over them.
        def start(rule):
            rule | CHARACTER
            def translator(node, scope):
                scope[name] = [first]
                subscope = scope.subscope()
                subscope[name].append(second)
                self.assertEqual(scope[name], [first, second])
                subscope[name] = [second]
                self.assertEqual(scope[name], [first, second])
                self.assertEqual(subscope[name], [second])
            rule.translator = translator
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        grammar.parse(pattern)
    
    @given(pattern=characters(), first=text(), second=text(), value=text(), default=text())
    def test_key_checks(self, pattern, first, second, value, default):
        # Scopes can be checked for the presence or absence of a key.
        assume(first != second)
        
        def start(rule):
            rule | CHARACTER
            def translator(node, scope):
                # A value in the parent scope
                scope[first] = value
                subscope = scope.subscope()
                self.assertTrue(first in scope)
                self.assertTrue(first in subscope)
                self.assertEqual(scope.get(first), value)
                self.assertEqual(subscope.get(first), value)
                self.assertEqual(scope.get(first, default), value)
                self.assertEqual(subscope.get(first, default), value)
                
                # A missing value
                self.assertFalse(second in scope)
                self.assertFalse(second in subscope)
                with self.assertRaises(KeyError):
                    scope[second]
                with self.assertRaises(KeyError):
                    subscope[second]
                self.assertEqual(scope.get(second), None)
                self.assertEqual(subscope.get(second), None)
                self.assertEqual(scope.get(second, default), default)
                self.assertEqual(subscope.get(second, default), default)
                
                # A value in the child scope
                subscope[second] = value
                self.assertFalse(second in scope)
                self.assertTrue(second in subscope)
                with self.assertRaises(KeyError):
                    scope[second]
                self.assertEqual(subscope[second], value)
                self.assertEqual(scope.get(second), None)
                self.assertEqual(subscope.get(second), value)
                self.assertEqual(scope.get(second, default), default)
                self.assertEqual(subscope.get(second, default), value)
                
                # A value in the both scopes
                scope[value] = first
                subscope[value] = second
                self.assertTrue(value in scope)
                self.assertTrue(value in subscope)
                self.assertEqual(scope.get(value), first)
                self.assertEqual(subscope.get(value), second)
                self.assertEqual(scope.get(value, default), first)
                self.assertEqual(subscope.get(value, default), second)
            rule.translator = translator
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        grammar.parse(pattern)
