from string import ascii_lowercase, ascii_uppercase
from unittest import TestCase

from hypothesis import assume, given
from hypothesis.strategies import characters, integers, lists, sets, text

from codewhisper.exceptions import ParseError, TokenizationError, WhisperException
from codewhisper.grammar import Grammar
from codewhisper.special import Lookahead, Plus, Seek, Star
from codewhisper.tokens import CHARACTER, EOF, CharacterRangeToken, CharacterToken, DelimitedToken, KeywordToken, PatternToken, StringToken, Token, join_tokens


class CharacterRangeTokenTests(TestCase):
    @given(codepoints=lists(integers(min_value=0, max_value=0x10FFFF), min_size=5, max_size=5))
    def test_check_range(self, codepoints):
        # CharacterRangeToken tokens should be selectable by token class.
        before, first_point, within, last_point, after = sorted(codepoints)
        
        class RANGE(CharacterRangeToken):
            first = first_point
            last = last_point
        
        def start(rule):
            rule | (RANGE)
        
        grammar = Grammar(start=start, tokens=[RANGE, CHARACTER])
        
        # A point in the middle is valid.
        char = chr(within)
        result = grammar.parse(char)
        self.assertEqual(result, [char])
        
        # The end points are always valid.
        char = chr(first_point)
        result = grammar.parse(char)
        self.assertEqual(result, [char])
        
        char = chr(last_point)
        result = grammar.parse(char)
        self.assertEqual(result, [char])
        
        if before < first_point:
            # Code points before the start of the range are invalid.
            with self.assertRaises(ParseError):
                grammar.parse(chr(before))
        
        if last_point < after:
            # Code points after the end of the range are invalid.
            with self.assertRaises(ParseError):
                grammar.parse(chr(after))
    
    @given(codepoints=lists(integers(min_value=0, max_value=0x10FFFF), min_size=4, max_size=4))
    def test_check_text(self, codepoints):
        # CharacterRangeToken tokens should be selectable by value.
        start_point, first, second, end_point = sorted(codepoints)
        assume(first != second)
        
        a = chr(first)
        b = chr(second)
        
        class RANGE(CharacterRangeToken):
            first = start_point
            last = end_point
        
        def start(rule):
            rule | (a, b)
        
        grammar = Grammar(start=start, tokens=[RANGE])
        
        # Valid input
        result = grammar.parse(a + b)
        self.assertEqual(result, [a, b])
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(b + a)
    
    @given(codepoints=lists(integers(min_value=0, max_value=0x10FFFF), min_size=7, max_size=7))
    def test_overlap(self, codepoints):
        # CharacterRangeTokens with overlapping ranges should map to the first one listed in the Grammar.
        first_start, first_point, second_start, shared_point, first_end, second_point, second_end = sorted(codepoints)
        assume(first_point < second_start <= first_end < second_point)
        first = chr(first_point)
        shared = chr(shared_point)
        second = chr(second_point)
        
        class FIRST(CharacterRangeToken):
            first = first_start
            last = first_end
        
        class SECOND(CharacterRangeToken):
            first = second_start
            last = second_end
        
        def start(rule):
            rule | (FIRST, SECOND)
        
        # With the FIRST token listed first, the shared codepoint is a FIRST token.
        grammar = Grammar(start=start, tokens=[FIRST, SECOND])
        self.assertEqual(grammar.parse(first + second), [first, second])
        self.assertEqual(grammar.parse(shared + second), [shared, second])
        with self.assertRaises(ParseError):
            grammar.parse(first + shared)
        
        # With the SECOND token listed first, the shared codepoint is a SECOND token.
        grammar = Grammar(start=start, tokens=[SECOND, FIRST])
        self.assertEqual(grammar.parse(first + second), [first, second])
        self.assertEqual(grammar.parse(first + shared), [first, shared])
        with self.assertRaises(ParseError):
            grammar.parse(shared + second)


class CharacterTokenTests(TestCase):
    @given(first=characters(), second=characters())
    def test_separate_tokens(self, first, second):
        assume(first != second)
        
        class FIRST(CharacterToken):
            characters = [first]
        
        class SECOND(CharacterToken):
            characters = second
        
        def start(rule):
            rule | (FIRST, SECOND)
        
        grammar = Grammar(start=start, tokens=[FIRST, SECOND])
        
        # Valid input
        result = grammar.parse(first + second)
        self.assertEqual(result, [first, second])
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(second + first)
    
    @given(first=characters(), second=characters())
    def test_same_token(self, first, second):
        assume(first != second)
        
        class CHAR(CharacterToken):
            characters = [first, second]
        
        def start(rule):
            rule | (first, second)
        
        grammar = Grammar(start=start, tokens=[CHAR])
        
        # Valid input
        result = grammar.parse(first + second)
        self.assertEqual(result, [first, second])
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(second + first)


class StringTokenTests(TestCase):
    @given(first=text(min_size=2), second=text(min_size=1))
    def test_separate_tokens(self, first, second):
        assume(not first.startswith(second))
        assume(not second.startswith(first))
        
        class FIRST(StringToken):
            strings = [first]
        
        class SECOND(StringToken):
            strings = [second]
        
        def start(rule):
            rule | (FIRST, SECOND)
        
        grammar = Grammar(start=start, tokens=[FIRST, SECOND])
        
        # Valid input
        result = grammar.parse(first + second)
        self.assertEqual(result, [first, second])
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(second + first)
    
    @given(first=text(min_size=2), second=text(min_size=1))
    def test_same_token(self, first, second):
        assume(not first.startswith(second))
        assume(not second.startswith(first))
        
        class STRING(StringToken):
            strings = [first, second]
        
        def start(rule):
            rule | (first, second)
        
        grammar = Grammar(start=start, tokens=[STRING])
        
        # Valid input
        result = grammar.parse(first + second)
        self.assertEqual(result, [first, second])
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(second + first)
    
    @given(prefix=text(min_size=1), suffix=text(min_size=1), tail=text())
    def test_longer_token_first(self, prefix, suffix, tail):
        # StringToken should match the longest string possible,
        # even when a shorter match appears later in the list of strings.
        assume(not tail.startswith(suffix))
        assume(prefix not in tail)
        full = prefix + suffix
        
        class STRING(StringToken):
            strings = [full, prefix]
        
        def start(rule):
            rule | (STRING, Star(CHARACTER))
        
        grammar = Grammar(start=start, tokens=[STRING, CHARACTER])
        
        # Shorter
        result = grammar.parse(prefix + tail)
        self.assertEqual(result, [prefix] + list(tail))
        
        # Longer
        result = grammar.parse(full + tail)
        self.assertEqual(result, [full] + list(tail))
    
    @given(prefix=text(min_size=1), suffix=text(min_size=1), tail=text())
    def test_shorter_token_first(self, prefix, suffix, tail):
        # StringToken should match the longest string possible,
        # even when a shorter match appears earlier in the list of strings.
        assume(not tail.startswith(suffix))
        assume(prefix not in tail)
        full = prefix + suffix
        
        class STRING(StringToken):
            strings = [prefix, full]
        
        def start(rule):
            rule | (STRING, Star(CHARACTER))
        
        grammar = Grammar(start=start, tokens=[STRING, CHARACTER])
        
        # Shorter
        result = grammar.parse(prefix + tail)
        self.assertEqual(result, [prefix] + list(tail))
        
        # Longer
        result = grammar.parse(full + tail)
        self.assertEqual(result, [full] + list(tail))
    
    @given(prefix=text(min_size=1), suffix=text(min_size=1), tail=text())
    def test_indeterminate_string_order(self, prefix, suffix, tail):
        # StringToken should match the longest string possible,
        # even when the order of strings might change.
        assume(not tail.startswith(suffix))
        assume(prefix not in tail)
        full = prefix + suffix
        
        class STRING(StringToken):
            strings = {prefix, full}
        
        def start(rule):
            rule | (STRING, Star(CHARACTER))
        
        grammar = Grammar(start=start, tokens=[STRING, CHARACTER])
        
        # Shorter
        result = grammar.parse(prefix + tail)
        self.assertEqual(result, [prefix] + list(tail))
        
        # Longer
        result = grammar.parse(full + tail)
        self.assertEqual(result, [full] + list(tail))
    
    @given(tokens=sets(text(min_size=1), min_size=1))
    def test_empty_string(self, tokens):
        # Strings in the sequence may not be empty.
        with self.assertRaises(WhisperException):
            class STRING(StringToken):
                strings = {""} | tokens
    
    @given(value=text(min_size=1))
    def test_empty_strings(self, value):
        # An empty sequence is allowed, but useless.
        class STRING(StringToken):
            strings = []
        
        def start(rule):
            rule | Seek(Lookahead(EOF))
        
        grammar = Grammar(start=start, tokens=[STRING, CHARACTER])
        
        result = grammar.parse(value)
        self.assertEqual(result, list(value))


class KeywordTokenTests(TestCase):
    alnum_text = text(characters(whitelist_categories=['L', 'N']), min_size=1)
    other_text = text(characters(blacklist_categories=['L', 'N']), min_size=1)
    
    @given(head=text(min_size=1), tail=alnum_text, separator=other_text)
    def test_trailing_characters(self, head, tail, separator):
        # KeywordToken subclasses cannot be followed by numbers or letters.
        assume(head not in tail + separator)
        assume(not head.startswith(tail))
        assume(not head.startswith(separator))
        
        class KEYWORD(KeywordToken):
            strings = [head]
        
        class OTHER(StringToken):
            strings = [separator, tail]
        
        def start(rule):
            rule | Plus({KEYWORD, OTHER})
        
        grammar = Grammar(start=start, tokens=[KEYWORD, OTHER])
        
        # Valid input
        result = grammar.parse(head + separator + tail)
        self.assertEqual(result, [head, separator, tail])
        
        # At end of input
        result = grammar.parse(tail + head)
        self.assertEqual(result, [tail, head])
        
        # Without the separator
        with self.assertRaises(TokenizationError):
            grammar.parse(head + tail)
    
    @given(first=alnum_text, second=alnum_text, separator=other_text)
    def test_order(self, first, second, separator):
        assume(not first.startswith(second))
        assume(not second.startswith(first))
        
        class STRING(StringToken):
            strings = [first, second]
        
        def start(rule):
            rule | (first, second)
        
        grammar = Grammar(start=start, tokens=[STRING, CHARACTER], ignore=[CHARACTER])
        
        # Valid input
        result = grammar.parse(first + separator + second)
        self.assertEqual(result, [first, second])
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(second + separator + first)
    
    @given(head=text(min_size=1), tail=alnum_text)
    def test_extended(self, head, tail):
        # KeywordToken strings can start with previous strings.
        class KEYWORD(KeywordToken):
            strings = [head, head + tail]
        
        def start(rule):
            rule | KEYWORD
        
        grammar = Grammar(start=start, tokens=[KEYWORD])
        
        # With the tail
        result = grammar.parse(head + tail)
        self.assertEqual(result, [head + tail])
        
        # Without the tail
        result = grammar.parse(head)
        self.assertEqual(result, [head])


class PatternTokenTests(TestCase):
    @given(upper=text(ascii_uppercase, min_size=1), lower=text(ascii_lowercase, min_size=1))
    def test_pattern_string(self, upper, lower):
        class UPPER(PatternToken):
            pattern = r'[A-Z]+'
        
        class LOWER(PatternToken):
            pattern = r'[a-z]+'
        
        def start(rule):
            rule | (UPPER, LOWER)
        
        grammar = Grammar(start=start, tokens=[UPPER, LOWER])
        
        # Valid input
        result = grammar.parse(upper + lower)
        self.assertEqual(result, [upper, lower])
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(lower + upper)
        
        # Completely invalid input
        with self.assertRaises(TokenizationError):
            result = grammar.parse(" ")
    
    @given(upper=text(ascii_uppercase, min_size=1), lower=text(ascii_lowercase, min_size=1))
    def test_compiled(self, upper, lower):
        # Patterns can be precompiled; for example, to use different flags.
        from re import compile
        
        class UPPER(PatternToken):
            pattern = compile(r'[A-Z]+')
        
        class LOWER(PatternToken):
            pattern = compile(r'[a-z]+')
        
        def start(rule):
            rule | (UPPER, LOWER)
        
        grammar = Grammar(start=start, tokens=[UPPER, LOWER])
        
        # Valid input
        result = grammar.parse(upper + lower)
        self.assertEqual(result, [upper, lower])
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(lower + upper)
    
    @given(head=text(ascii_uppercase, min_size=1), inner=characters(whitelist_categories=['Ll']), tail=text(ascii_uppercase, min_size=1))
    def test_regex_package(self, head, inner, tail):
        # Patterns can be precompiled by the regex package.
        from regex import compile
        
        class UPPER(PatternToken):
            pattern = compile(r'(?:[A-Z]+){i<=1}')
        
        def start(rule):
            rule | UPPER
        
        grammar = Grammar(start=start, tokens=[UPPER])
        
        # Valid input
        result = grammar.parse(head + inner + tail)
        self.assertEqual(result, [head + inner + tail])
        
        # Too many insertions
        with self.assertRaises(TokenizationError):
            grammar.parse(head + inner + inner + tail)
    
    @given(allowed=text(min_size=1))
    def test_arbitrary_characters(self, allowed):
        assume('-' not in allowed)
        assume('^' not in allowed)
        assume('[' not in allowed)
        assume(']' not in allowed)
        assume('\\' not in allowed)
        
        class CHAR(PatternToken):
            pattern = f"[{allowed}]"
        
        def start(rule):
            rule | CHAR
        
        grammar = Grammar(start=start, tokens=[CHAR])
        
        for c in allowed:
            result = grammar.parse(c)
            self.assertEqual(result, [c])


class DelimitedTokenTests(TestCase):
    @given(starting=text(min_size=1), middle=text(), ending=text(min_size=1))
    def test_unescaped(self, starting, middle, ending):
        assume(ending not in middle + ending[:-1])
        
        class STRING(DelimitedToken):
            start = starting
            end = ending
        
        def start(rule):
            rule | STRING
        
        grammar = Grammar(start=start, tokens=[STRING])
        
        # Valid input
        text = starting + middle + ending
        result = grammar.parse(text)
        self.assertEqual(result, [text])
        
        # Missing the final delimiter
        with self.assertRaises(TokenizationError):
            result = grammar.parse(starting + middle)
    
    @given(starting=text(min_size=1), middle=text(), escaping=text(min_size=1), ending=text(min_size=1))
    def test_escaped(self, starting, middle, escaping, ending):
        assume(ending not in middle + ending[:-1])
        assume(ending not in middle + escaping + ending[:-1])
        assume(escaping not in middle)
        assume(not ending.startswith(escaping))
        
        class STRING(DelimitedToken):
            start = starting
            end = ending
            escape = escaping
        
        def start(rule):
            rule | STRING
        
        grammar = Grammar(start=start, tokens=[STRING])
        
        # Valid input
        text = starting + middle + escaping + ending + middle + ending
        result = grammar.parse(text)
        self.assertEqual(result, [text])
        
        # Escaping the only final delimiter
        with self.assertRaises(TokenizationError):
            result = grammar.parse(starting + middle + escaping + ending)
    
    @given(starting=text(min_size=1), middle=text(), escaping=text(min_size=1), ending=text(min_size=1), times=integers(min_value=0, max_value=5))
    def test_double_escaped(self, starting, middle, escaping, ending, times):
        # The string should be able to end with the escape sequence, by escaping that.
        assume(ending not in middle + ending[:-1])
        assume(ending not in middle + escaping + ending[:-1])
        assume(ending not in middle + escaping + escaping + ending[:-1])
        assume(escaping not in middle + ending)
        
        class STRING(DelimitedToken):
            start = starting
            end = ending
            escape = escaping
        
        def start(rule):
            rule | STRING
        
        grammar = Grammar(start=start, tokens=[STRING])
        
        # Even number of escapes
        text = starting + middle + (escaping * (times * 2)) + ending
        result = grammar.parse(text)
        self.assertEqual(result, [text])
        
        # Odd number of escapes
        text = starting + middle + (escaping * (times * 2 + 1)) + ending + ending
        result = grammar.parse(text)
        self.assertEqual(result, [text])
    
    @given(starting=text(min_size=1), first=text(), second=text(), ending=text(min_size=1), separator=text(min_size=1))
    def test_multiple(self, starting, first, second, ending, separator):
        assume(ending not in first + ending[:-1])
        assume(ending not in second + ending[:-1])
        assume(starting not in separator + starting[:-1])
        
        class STRING(DelimitedToken):
            start = starting
            end = ending
        
        class SEPARATOR(StringToken):
            strings = [separator]
        
        def start(rule):
            rule | (STRING, separator, STRING)
        
        grammar = Grammar(start=start, tokens=[STRING, SEPARATOR])
        
        result = grammar.parse(starting + first + ending + separator + starting + second + ending)
        self.assertEqual(result, [starting + first + ending, separator, starting + second + ending])


class CustomTokenTests(TestCase):
    @given(before=text(min_size=1), after=text(min_size=1))
    def test_zero_width_token(self, before, after):
        # Token classes should be able to match the empty string at any position before the end,
        # allowing subsequent token classes to attempt a match at the same position.
        assume(not (before + after).startswith(after))
        
        class NEVER(Token):
            @staticmethod
            def match(text, position):
                return None
        
        class ITEM(StringToken):
            strings = {before, after}
        
        class EMPTY(Token):
            @staticmethod
            def match(text, position):
                return 0
        
        def start(rule):
            rule | Plus(EMPTY, ITEM)
        
        grammar = Grammar(start=start, tokens=[NEVER, EMPTY, ITEM])
        tokens = grammar.tokenize(before + after)
        token_classes = [token.__class__ for token in tokens]
        self.assertEqual(token_classes, [EMPTY, ITEM, EMPTY, ITEM, EOF])
        token_values = [token.value for token in tokens]
        self.assertEqual(token_values, ["", before, "", after, ""])


class TokenJoiningTests(TestCase):
    @given(words=lists(text()))
    def test_join_values(self, words):
        # The value of each listed token should be concatenated.
        class TOKEN(Token):
            pass
        
        tokens = [TOKEN(word, 1, n, [], 0) for n, word in enumerate(words)]
        result = join_tokens(tokens)
        self.assertEqual(result, ''.join(words))
    
    @given(first=text(), second=text(), words=lists(text()))
    def test_ignored(self, first, second, words):
        # Ignored tokens between the listed tokens should be included.
        class TOKEN(Token):
            pass
        
        class IGNORED(Token):
            pass
        
        ignored = [IGNORED(word, 1, n + 1, [], 0) for n, word in enumerate(words)]
        tokens = [TOKEN(first, 1, 0, [], 0), TOKEN(second, 2, 0, ignored, 0)]
        result = join_tokens(tokens)
        self.assertEqual(result, first + ''.join(words) + second)
    
    @given(first=text(), second=text(), words=lists(text()))
    def test_initial_ignored(self, first, second, words):
        # Ignored tokens before the first token should be omitted.
        class TOKEN(Token):
            pass
        
        class IGNORED(Token):
            pass
        
        ignored = [IGNORED(word, 1, n + 1, [], 0) for n, word in enumerate(words)]
        tokens = [TOKEN(first, 1, 0, ignored, 0), TOKEN(second, 2, 0, [], 0)]
        result = join_tokens(tokens)
        self.assertEqual(result, first + second)
    
    @given(value=text(), whitespace=sets(characters(), min_size=1))
    def test_strip(self, value, whitespace):
        # Ignored characters get stripped from both ends.
        class WHITESPACE(CharacterToken):
            characters = whitespace
        
        def start(rule):
            rule | Star(CHARACTER)
            def translate(node, scope):
                return join_tokens(node)
            rule.translator = translate
        
        grammar = Grammar(start, tokens=[WHITESPACE, CHARACTER], ignore=[WHITESPACE])
        result = grammar.parse(value)
        self.assertEqual(result, value.strip(''.join(whitespace)))
    
    @given(value=text(), whitespace=sets(characters(), min_size=1))
    def test_final_whitespace(self, value, whitespace):
        # Ignored tokens can be collected from the EOF.
        class WHITESPACE(CharacterToken):
            characters = whitespace
        
        def start(rule):
            rule | (Star(CHARACTER), EOF)
            def translate(node, scope):
                return join_tokens(node)
            rule.translator = translate
        
        grammar = Grammar(start, tokens=[WHITESPACE, CHARACTER], ignore=[WHITESPACE])
        result = grammar.parse(value)
        self.assertEqual(result, value.lstrip(''.join(whitespace)))
