from unittest import TestCase

from hypothesis import assume, given
from hypothesis import strategies as st
from hypothesis.strategies import characters, sets, text

from codewhisper.exceptions import ParseError, RuleError
from codewhisper.grammar import Grammar
from codewhisper.parsing import Node
from codewhisper.special import Plus
from codewhisper.tokens import CHARACTER, EOF, CharacterToken


nonrule_value = (
    st.binary() |
    st.booleans() |
    st.complex_numbers() |
    st.dates() |
    st.datetimes() |
    st.decimals() |
    st.dictionaries(text(), text()) |
    st.floats() |
    st.fractions() |
    st.integers() |
    st.none()
)

class RuleTests(TestCase):
    @given(character=characters(), other=characters(), tail=text(min_size=1))
    def test_character(self, character, other, tail):
        # A rule expecting a particular character should accept that one, but not another.
        assume(character != other)
        
        def start(rule):
            rule | character
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Valid input
        result = grammar.parse(character)
        self.assertEqual(result, [character])
        
        # Invalid input
        with self.assertRaises(ParseError):
            result = grammar.parse(other)
    
    @given(first=characters(), second=characters())
    def test_order(self, first, second):
        # A rule can expect two characters in order.
        assume(first != second)
        
        def start(rule):
            rule | (first, second)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Valid input
        result = grammar.parse(first + second)
        self.assertEqual(result, list(first + second))
        
        # Reversed input
        with self.assertRaises(ParseError):
            result = grammar.parse(second + first)
    
    @given(head=text(min_size=1), tail=text(min_size=1))
    def test_short_input(self, head, tail):
        def start(rule):
            rule | tuple(head + tail)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Partial input
        with self.assertRaises(ParseError):
            grammar.parse(head)
        
        # Empty string
        with self.assertRaises(ParseError):
            grammar.parse("")
    
    @given(head=text(min_size=1), tail=text(min_size=1))
    def test_long_input(self, head, tail):
        def start(rule):
            rule | tuple(head)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        with self.assertRaises(ParseError):
            grammar.parse(head + tail)
    
    def test_without_alternatives(self):
        def start(rule):
            pass
        
        with self.assertRaises(RuleError):
            Grammar(start=start, tokens=[CHARACTER])
    
    @given(value=nonrule_value)
    def test_invalid_rule_items(self, value):
        def start(rule):
            rule | value
        
        with self.assertRaises(RuleError):
            Grammar(start=start, tokens=[CHARACTER])
    
    @given(head=characters(), optional=text(min_size=1), tail=characters())
    def test_optional_items(self, head, optional, tail):
        # Rule items in a list are optional.
        assume(not tail.startswith(optional))
        
        def start(rule):
            rule | (head, list(optional), tail)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # With the optional part
        with_optional = head + optional + tail
        result = grammar.parse(with_optional)
        self.assertEqual(result, list(with_optional))
        
        # Without the optional part
        without_optional = head + tail
        result = grammar.parse(without_optional)
        self.assertEqual(result, list(without_optional))
    
    @given(head=characters(), middle=sets(characters(), min_size=1), tail=characters())
    def test_item_sets(self, head, middle, tail):
        # Rule items in a set match an arbitrary one of the items.
        
        def start(rule):
            rule | (head, middle, tail)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        for char in middle:
            string = head + char + tail
            result = grammar.parse(string)
            self.assertEqual(result, list(string))
    
    @given(matched=characters(), other=characters())
    def test_unmatched_sets(self, matched, other):
        # Sets don't match items not in the set.
        assume(matched != other)
        
        def start(rule):
            rule | {matched}
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        with self.assertRaises(ParseError):
            grammar.parse(other)
    
    @given(first=characters(), second=characters(), third=characters())
    def test_subrule(self, first, second, third):
        # A rule should be able to refer to other rules.
        assume(third != first)
        assume(third != second)
        
        def start(rule):
            rule | (subrule, subrule)
        
        def subrule(rule):
            rule | first
            rule | second
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second)
        self.assertEqual(result, [[first], [second]])
        
        result = grammar.parse(second + first)
        self.assertEqual(result, [[second], [first]])
        
        with self.assertRaises(ParseError):
            grammar.parse(first + third)
    
    @given(first=characters(), second=characters(), third=characters())
    def test_repeated_subrule(self, first, second, third):
        # Different options may start with the same subrule.
        assume(third != second)
        
        def start(rule):
            rule | (subrule, second)
            rule | (subrule, third)
        
        def subrule(rule):
            rule | first
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second)
        self.assertEqual(result, [[first], second])
        
        result = grammar.parse(first + third)
        self.assertEqual(result, [[first], third])
    
    @given(value=text(min_size=1))
    def test_right_recursion(self, value):
        # A rule should be able to refer to itself.
        def start(rule):
            rule | (CHARACTER, [start])
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(value)
        expected = [value[-1]]
        for c in value[-2::-1]:
            expected = [c, expected]
        self.assertEqual(result, expected)
    
    @given(used=characters(), ignored=characters())
    def test_ignored(self, used, ignored):
        assume(used != ignored)
        
        class IGNORED(CharacterToken):
            characters = ignored
        
        def start(rule):
            rule | (CHARACTER, CHARACTER)
        
        grammar = Grammar(start=start, tokens=[IGNORED, CHARACTER], ignore=[IGNORED])
        
        # Ignored at the beginning of the text
        result = grammar.parse(ignored + used + used)
        self.assertEqual(result, [used, used])
        
        # Ignored in the middle of the text
        result = grammar.parse(used + ignored + used)
        self.assertEqual(result, [used, used])
        
        # Ignored at the end of the text
        result = grammar.parse(used + used + ignored)
        self.assertEqual(result, [used, used])
    
    @given(first=characters(), second=characters(), third=characters(), mark=st.integers())
    def test_path_preference(self, first, second, third, mark):
        # Translation prefers the first available option in each rule.
        def start(rule):
            rule | ([CHARACTER], subrule)
            rule | (subrule, [CHARACTER, deeper])
            rule | (CHARACTER, CHARACTER, CHARACTER)
        
        def subrule(rule):
            rule | (CHARACTER, CHARACTER, deeper)
            rule | (deeper, [CHARACTER])
        
        def deeper(rule):
            rule | CHARACTER
            rule.translator = lambda node, scope: mark
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + second + third)
        self.assertEqual(result, [first, [mark, third]])

    @given(head=characters(), tail=characters())
    def test_backup_paths(self, head, tail):
        # If the first pathway through a rule doesn't work with what follows, others should be tried.
        def start(rule):
            rule | (subrule, tail)
        
        def subrule(rule):
            rule | (head, tail)
            rule | (head)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(head + tail)
        self.assertEqual(result, [[head], tail])

    @given(text=text(min_size=2, max_size=3))
    def test_finish_input(self, text):
        # If the start rule successfully parses part of the text, it should be asked to keep going.
        def start(rule):
            rule | (CHARACTER)
            rule | (CHARACTER, CHARACTER)
            rule | (CHARACTER, CHARACTER, CHARACTER)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(text)
        self.assertEqual(result, list(text))

    @given(text=text(min_size=1))
    def test_consumes_eof(self, text):
        # The start rule may consume the EOF token at the end of the token stream.
        def start(rule):
            rule | (Plus(CHARACTER), EOF)
            rule.result = [CHARACTER]
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(text)
        self.assertEqual(result, list(text))

    @given(first=characters(), second=characters())
    def test_empty_tuple(self, first, second):
        assume(first != second)
        
        def start(rule):
            rule | (first, empty, second)
            rule | (empty, second)
            rule | (first, empty)
        
        def empty(rule):
            rule | ()
        
        # At the start of a rule
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(second)
        self.assertEqual(result, [[], second])
        
        # At the end of a rule
        result = grammar.parse(first)
        self.assertEqual(result, [first, []])
        
        # In the middle of a rule
        result = grammar.parse(first + second)
        self.assertEqual(result, [first, [], second])
        
        # As the starting rule
        grammar = Grammar(start=empty, tokens=[CHARACTER])
        result = grammar.parse('')
        self.assertEqual(result, [])

    @given(chars=sets(characters(), min_size=2), selected=st.integers(min_value=1))
    def test_multiple_optional(self, chars, selected):
        sequence = list(chars)
        
        def start(rule):
            rule | subrule
            rule.single = True
        
        def subrule(rule):
            rule | tuple([c] for c in sequence)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # All characters
        result = grammar.parse(''.join(sequence))
        self.assertEqual(result, sequence)
        
        # No characters
        result = grammar.parse('')
        self.assertEqual(result, [])
        
        # Some characters
        some = []
        for c in sequence:
            if selected & 1:
                some.append(c)
            selected >>= 1
        result = grammar.parse(''.join(some))
        self.assertEqual(result, some)
        
        # Duplicate characters
        text = ''.join(c + c for c in some or sequence)
        with self.assertRaises(ParseError):
            result = grammar.parse(text)
        
        # Reversed
        with self.assertRaises(ParseError):
            result = grammar.parse(''.join(reversed(sequence)))
        
        if len(some) > 1:
            with self.assertRaises(ParseError):
                result = grammar.parse(''.join(reversed(some)))
    
    @given(value=text(min_size=1))
    def test_left_recursion(self, value):
        # Left-recursive rules shouldn't crash, but might not fully work.
        def start(rule):
            rule | (start, CHARACTER)
            rule | (CHARACTER)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        try:
            result = grammar.parse(value)
        except ParseError:
            pass
        else:
            expected = [value[0]]
            for c in value[1:]:
                expected = [expected, c]
            self.assertEqual(result, expected)
    
    @given(value=text(min_size=1))
    def test_left_recursion_after(self, value):
        # A rule should always be able to refer to its prior results.
        def start(rule):
            rule | (CHARACTER)
            rule | (start, CHARACTER)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(value)
        expected = [value[0]]
        for c in value[1:]:
            expected = [expected, c]
        self.assertEqual(result, expected)

    def test_center_recursion(self):
        # The parser should recognize unambiguous nondeterministic CFG rules,
        # even those not accepted by a normal PEG packrat parser.
        def start(rule):
            rule | (CHARACTER, start, CHARACTER)
            rule | (CHARACTER)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse("x")
        self.assertEqual(result, ["x"])
        
        result = grammar.parse("xxx")
        self.assertEqual(result, ["x", ["x"], "x"])
        
        # Some algorithms would have trouble here, because the best result for `start`
        # starting with character 4 consumes three characters instead of one, and
        # at character 3 it wants to consume five characters instead of three.
        # https://pdos.csail.mit.edu/~baford/packrat/icfp02/packrat-icfp02.pdf §4.3
        result = grammar.parse("x" * 7)
        self.assertEqual(result, ["x", ["x", ["x", ["x"], "x"], "x"], "x"])
        
        with self.assertRaises(ParseError):
            grammar.parse("x" * 4)


class RuleResultTests(TestCase):
    @given(first=characters(), second=characters(), third=characters())
    def test_default(self, first, second, third):
        # With no parameters, a rule translates to a list of tokens and subrule translations,
        # including tokens in lists, tuples, and sets.
        def start(rule):
            rule | ({first}, subrule)
        
        def subrule(rule):
            rule | (second, [third])
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second + third)
        self.assertEqual(result, [first, [second, third]])
    
    @given(first=characters(), second=characters(), third=characters())
    def test_result_dict(self, first, second, third):
        # With a dictionary of attributes, a rule translates to a dict.
        # In the absence of an explicit translator, each value in the result gets translated.
        def start(rule):
            rule | ({first}, subrule)
            rule.result = {
                'subrule': subrule,
                'single': {CHARACTER, subrule},
                'list': [CHARACTER, subrule],
            }
        
        def subrule(rule):
            rule | (second, [third])
            rule.result = {
                'token': CHARACTER,
                'list': [CHARACTER],
            }
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second + third)
        subresult = {'token': second, 'list': [second, third]}
        self.assertEqual(result, {
            'subrule': subresult,
            'single': first,
            'list': [first, subresult],
        })
    
    @given(first=characters(), second=characters(), third=characters())
    def test_result_list(self, first, second, third):
        # Rules may translate to a list of specific rules or token types.
        def start(rule):
            rule | ({first}, subrule)
            rule.result = [subrule]
        
        def subrule(rule):
            rule | (second, [third])
            rule.result = [CHARACTER]
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second + third)
        self.assertEqual(result, [[second, third]])
    
    @given(first=characters(), second=characters(), third=characters())
    def test_result_set(self, first, second, third):
        # Rules may translate to the first of specific rules or token types.
        def start(rule):
            rule | ({first}, subrule)
            rule.result = {subrule}
        
        def subrule(rule):
            rule | (second, [third])
            rule.result = {CHARACTER}
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second + third)
        self.assertEqual(result, second)
    
    @given(first=characters(), second=characters(), third=characters())
    def test_result_single(self, first, second, third):
        # Rules may translate to the first of a single rule or token type.
        def start(rule):
            rule | ({first}, subrule)
            rule.result = subrule
        
        def subrule(rule):
            rule | (second, [third])
            rule.result = CHARACTER
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second + third)
        self.assertEqual(result, second)
    
    @given(first=characters(), second=characters(), third=characters())
    def test_single(self, first, second, third):
        # Rules may translate to the first item of any type.
        def start(rule):
            rule | ({first}, subrule)
            rule.single = False
        
        def subrule(rule):
            rule | ([second, third])
            rule.single = True
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second + third)
        self.assertEqual(result, [first, second])
        
        # With no items, the end result is None.
        result = grammar.parse(first)
        self.assertEqual(result, [first, None])
    
    @given(first=characters(), second=characters(), third=characters())
    def test_translator(self, first, second, third):
        # Rules may use a function to translate the results.
        def start(rule):
            rule | ({first}, subrule)
            rule.result = {
                'subrule': subrule,
                'single': {CHARACTER, subrule},
                'list': [CHARACTER, subrule],
            }
            rule.translator = lambda node, scope: {
                'a': node['single'].value,
                'b': scope.translate(node['subrule']),
            }
        
        def subrule(rule):
            rule | (second, [third])
            def subrule_translator(values, scope):
                return {'tokens': scope.translate(values)}
            rule.translator = subrule_translator
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(first + second + third)
        self.assertEqual(result, {
            'a': first,
            'b': {'tokens': [second, third]},
        })
    
    def test_deep(self):
        def start(rule):
            rule | Plus({first, second})
            rule.result = {
                'first': [first],
                'second': [second],
            }
            def translate_start(node, scope):
                self.assertIsInstance(node['first'], list)
                self.assertEqual(len(node['first']), 2)
                for pair in node['first']:
                    self.assertIsInstance(pair, list)
                    self.assertEqual(len(pair), 2)
                    first, second = pair
                    self.assertIsInstance(first, CHARACTER)
                    self.assertIsInstance(second, Node)
                return {
                    'first': scope.translate(node['first']),
                    'second': scope.translate(node['second']),
                }
            rule.translator = translate_start
        
        def first(rule):
            rule | ('a', third)
        
        def second(rule):
            rule | ('b', third)
            rule.single = True
        
        def third(rule):
            rule | 'c'
            rule.result = {'c': [CHARACTER]}
            rule.translator = lambda node, scope: node['c']
        
        result = Grammar(start=start, tokens=[CHARACTER]).parse("acbcac")
        self.assertEqual(result, {
            'first': [['a', [CHARACTER('c', 1, 2, [], 0)]], ['a', [CHARACTER('c', 1, 6, [], 0)]]],
            'second': ['b'],
        })
