from unittest import TestCase

from hypothesis import assume, example, given
from hypothesis.strategies import booleans, characters, composite, integers, lists, none, permutations, text

from codewhisper.exceptions import ParseError, RuleError
from codewhisper.grammar import Grammar
from codewhisper.special import Avoid, Commas, Consume, Each, Lookahead, Max, Notahead, Parallel, Plus, Seek, Sequence, Some, Star
from codewhisper.tokens import CHARACTER, StringToken, join_tokens


@composite
def reordered_items(draw, base=text(min_size=2)):
    items = draw(base)
    reordered = draw(permutations(items))
    length = draw(integers(min_value=1, max_value=len(items) - 1))
    return items, reordered, length


class AvoidTests(TestCase):
    @given(head=text(), items=text(min_size=1), times=integers(min_value=2, max_value=5), tail=text())
    def test_parsing(self, head, items, times, tail):
        # Avoid() rule items may be omitted.
        def start(rule):
            rule | (tuple(head), Avoid(*items), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Included
        text = head + items + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Omitted
        text = head + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Multiple times
        text = head + (items * times) + tail
        with self.assertRaises(ParseError):
            grammar.parse(text)
    
    @given(item=text(min_size=1), tail=text())
    def test_greed(self, item, tail):
        # Avoid() tries to avoid matching, if at all possible.
        def start(rule):
            rule | (Avoid(subrule), Star(CHARACTER))
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | tuple(item)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(item + tail)
        self.assertEqual(result, 0)
    
    @given(first=characters(), second=characters())
    def test_attributable(self, first, second):
        # Items within Avoid() can be included in a rule's result.
        def start(rule):
            rule | (Avoid(subrule), Avoid(second))
            rule.result = {
                'subrule': subrule,
                'token': CHARACTER,
            }
        
        def subrule(rule):
            rule | first
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + second)
        self.assertEqual(result, {'subrule': [first], 'token': second})


class CommasTests(TestCase):
    @given(values=lists(text(min_size=1), min_size=1))
    def test_defaults(self, values):
        for value in values:
            assume("," not in value)
        
        class VALUE(StringToken):
            strings = sorted(values, reverse=True)
        
        def start(rule):
            rule | Commas(VALUE)
            rule.result = [VALUE]
        
        grammar = Grammar(start, tokens=[VALUE, CHARACTER])
        
        # Without a trailing comma
        text = ",".join(values)
        result = grammar.parse(text)
        self.assertEqual(result, values)
        
        # With a trailing comma
        with self.assertRaises(ParseError):
            grammar.parse(text + ",")
    
    @given(values=lists(text(min_size=1), min_size=1), separator=text(min_size=1))
    def test_separator(self, values, separator):
        # A custom separator value may be given.
        for value in values:
            assume(separator not in value)
            assume(value not in separator)
        
        class SEPARATOR(StringToken):
            strings = [separator]
        
        class VALUE(StringToken):
            strings = sorted(values, reverse=True)
        
        # Given as a string type
        def start(rule):
            rule | Commas(VALUE, separator=SEPARATOR)
            rule.result = [VALUE]
        grammar = Grammar(start, tokens=[VALUE, SEPARATOR, CHARACTER])
        result = grammar.parse(separator.join(values))
        self.assertEqual(result, values)
        
        # Given as a token type
        def start(rule):
            rule | Commas(VALUE, separator=separator)
            rule.result = [VALUE]
        grammar = Grammar(start, tokens=[VALUE, SEPARATOR, CHARACTER])
        result = grammar.parse(separator.join(values))
        self.assertEqual(result, values)
        
        if separator != "," and len(values) > 1:
            # Using commas instead of the separator
            with self.assertRaises(ParseError):
                grammar.parse(",".join(values))
    
    @given(values=lists(text(min_size=1), min_size=1), separator=text(min_size=1))
    def test_trailing(self, values, separator):
        for value in values:
            assume("," not in value)
        
        class VALUE(StringToken):
            strings = sorted(values, reverse=True)
        
        def start(rule):
            rule | Commas(VALUE, trailing=True)
            rule.result = [VALUE]
        
        grammar = Grammar(start, tokens=[VALUE, CHARACTER])
        
        # Without a trailing comma
        text = ",".join(values)
        result = grammar.parse(text)
        self.assertEqual(result, values)
        
        # With a trailing comma
        result = grammar.parse(text + ",")
        self.assertEqual(result, values)


class ConsumeTests(TestCase):
    @given(head=text(), items=text(min_size=1), times=integers(min_value=2, max_value=5), tail=text())
    def test_parsing(self, head, items, times, tail):
        # Consume() rule items may be omitted.
        assume(not tail.startswith(items))
        
        def start(rule):
            rule | (tuple(head), Consume(*items), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Included
        text = head + items + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Omitted
        text = head + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Multiple times
        text = head + (items * times) + tail
        with self.assertRaises(ParseError):
            grammar.parse(text)
    
    @given(item=text(min_size=1), tail=text())
    def test_greed(self, item, tail):
        # Consume() always matches if possible.
        def start(rule):
            rule | (Consume(subrule), Star(CHARACTER))
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | tuple(item)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(item + tail)
        self.assertEqual(result, 1)
    
    @given(head=text(), item=text(min_size=1), tail=text())
    def test_unavoidable(self, head, item, tail):
        # Consume() sequences can't be saved for the parent sequence to match.
        def start(rule):
            rule | (subrule, first)
            rule | (subrule, second)
            rule.result = {first, second}
        
        def subrule(rule):
            rule | (tuple(head), Consume(*item))
        
        def first(rule):
            rule | (tuple(item), tuple(tail))
            rule.translator = lambda node, scope: 'first'
        
        def second(rule):
            rule | tuple(tail)
            rule.translator = lambda node, scope: 'second'
        
        # With the sequence
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(head + item + tail)
        self.assertEqual(result, 'second')
    
    @given(first=characters(), second=characters())
    def test_attributable(self, first, second):
        # Items within Consume() can be included in a rule's result.
        def start(rule):
            rule | (Consume(subrule), Consume(second))
            rule.result = {
                'subrule': subrule,
                'token': CHARACTER,
            }
        
        def subrule(rule):
            rule | first
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + second)
        self.assertEqual(result, {'subrule': [first], 'token': second})


class EachTests(TestCase):
    @given(head=text(), items=reordered_items(), tail=text())
    def test_order(self, head, items, tail):
        # Each() rule items may appear in any order.
        inner, reordered, length = items
        
        def start(rule):
            rule | (tuple(head), Each(*reordered), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = head + inner + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
    
    @given(head=text(), items=reordered_items(), tail=text())
    def test_partial(self, head, items, tail):
        # Each() rule items must all appear.
        inner, reordered, length = items
        
        def start(rule):
            rule | (tuple(head), Each(*reordered), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = head + inner[:length] + tail
        with self.assertRaises(ParseError):
            grammar.parse(text)
    
    @given(head=text(), items=text(min_size=2), tail=text())
    def test_optional(self, head, items, tail):
        # Each() rule items aren't optional.
        def start(rule):
            rule | (tuple(head), Each(*items), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = head + tail
        with self.assertRaises(ParseError):
            grammar.parse(text)
    
    @given(first=characters(), second=characters())
    def test_attributable(self, first, second):
        # Items within Each() can be included in a rule's result.
        def start(rule):
            rule | Each(subrule, second)
            rule.result = {
                'subrule': subrule,
                'token': CHARACTER,
            }
        
        def subrule(rule):
            rule | first
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + second)
        self.assertEqual(result, {'subrule': [first], 'token': second})
        
        result = grammar.parse(second + first)
        self.assertEqual(result, {'subrule': [first], 'token': second})


class LookaheadTests(TestCase):
    @given(a=characters(), b=characters(), c=characters(), d=characters())
    def test_parsing(self, a, b, c, d):
        # Lookahead() passes without progressing if the sequence matches at the current location.
        assume(c != d)
        
        def start(rule):
            rule | (subrule, subrule)
        
        def subrule(rule):
            rule | (a, Lookahead(b, c), count)
            rule | (a, b, count)
            rule.result = {count}
        
        def count(rule):
            rule | (CHARACTER, [CHARACTER])
            rule.translator = lambda node, scope: len(node)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = a + b + c + a + b + d
        result = grammar.parse(text)
        self.assertEqual(result, [2, 1])
    
    @given(a=characters(), b=characters(), c=characters())
    def test_shortcut(self, a, b, c):
        # Lookahead shouldn't prevent future subrule calls from iterating over all possibilities.
        def start(rule):
            rule | (Lookahead(subrule), subrule)
        
        def subrule(rule):
            rule | (a)
            rule | (a, b)
            rule | (a, b, c)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(a + b + c)
        self.assertEqual(result, [[a, b, c]])


class MaxTests(TestCase):
    @given(head=text(), items=text(min_size=1), times=integers(min_value=2, max_value=5), tail=text())
    def test_parsing(self, head, items, times, tail):
        # Max() rule items can be listed one or more times.
        def start(rule):
            rule | (tuple(head), Max(*items), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Once
        text = head + items + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Multiple times
        text = head + (items * times) + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Zero times
        with self.assertRaises(ParseError):
            grammar.parse(head + tail)
    
    @given(item=text(min_size=1), times=integers(min_value=2, max_value=5), tail=text())
    def test_greed(self, item, times, tail):
        # Max() tries to match as many times as possible.
        assume(not tail.startswith(item))
        
        def start(rule):
            rule | (Max(subrule), Star(CHARACTER))
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | tuple(item)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Once
        result = grammar.parse(item + tail)
        self.assertEqual(result, 1)
        
        # Multiple times
        text = (item * times) + tail
        result = grammar.parse(text)
        self.assertEqual(result, times)
    
    @given(first=text(min_size=1), second=text(min_size=1))
    def test_greed_restriction(self, first, second):
        # Max() should prefer more iterations over longer items.
        # (This is the main difference between Max and Plus())
        assume(not first.startswith(second + second))
        assume(not second.startswith(first))
        
        def start(rule):
            rule | Max(subrule)
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | (tuple(first), [tuple(second)])
            rule | (tuple(second))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + second + second)
        self.assertEqual(result, 3)
        
        result = grammar.parse(first + second)
        self.assertEqual(result, 2)
    
    @given(first=text(min_size=1), second=text(min_size=1))
    def test_ambiguity(self, first, second):
        # Max() can return fewer iterations if necessary.
        assume(not second.startswith(first))
        
        def start(rule):
            rule | (Max(subrule), tuple(first + second))
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | tuple(first)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + first + second)
        self.assertEqual(result, 1)


class NotaheadTests(TestCase):
    @given(a=characters(), b=characters(), c=characters(), d=characters())
    def test_parsing(self, a, b, c, d):
        # Notahead() passes without progressing if the sequence matches at the current location.
        assume(c != d)
        
        def start(rule):
            rule | (subrule, subrule)
        
        def subrule(rule):
            rule | (a, Notahead(b, d), count)
            rule | (a, b, count)
            rule.result = {count}
        
        def count(rule):
            rule | (CHARACTER, [CHARACTER])
            rule.translator = lambda node, scope: len(node)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = a + b + c + a + b + d
        result = grammar.parse(text)
        self.assertEqual(result, [2, 1])
    
    @given(a=characters(), b=characters(), c=characters())
    def test_shortcut(self, a, b, c):
        # Notahead shouldn't prevent future subrule calls from iterating over all possibilities.
        def start(rule):
            rule | (Notahead(subrule), CHARACTER)
            rule | (subrule, c)
        
        def subrule(rule):
            rule | (a)
            rule | (a, b)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(a + b + c)
        self.assertEqual(result, [[a, b], c])


class ParallelTests(TestCase):
    @given(head=text(), first=text(min_size=1), second=text(min_size=1), tail=text())
    def test_parsing(self, head, first, second, tail):
        # Parallel() should match each of its sequences, at the same time.
        def start(rule):
            rule | (tuple(head), Parallel((one, two), both), tuple(tail))
            rule | (tuple(head), both, tuple(tail))
            rule.result = {
                'one': one,
                'two': two,
                'both': both,
            }
        
        def one(rule):
            rule | tuple(first)
        
        def two(rule):
            rule | tuple(second)
        
        def both(rule):
            rule | tuple(first + second)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = head + first + second + tail
        result = grammar.parse(text)
        self.assertEqual(result, {
            'one': list(first),
            'two': list(second),
            'both': list(first + second),
        })
        
        # Multiple matches should not be allowed.
        with self.assertRaises(ParseError):
            grammar.parse(head + first + second + first + second + tail)
        
        # A match is not optional.
        with self.assertRaises(ParseError):
            grammar.parse(head + tail)
    
    @given(value=text())
    @example(value="")
    @example(value="1234567")
    @example(value="abcdefghijklmnopqrstuvwxyz")
    def test_length(self, value):
        # Parallel() should only allow matches of equal length.
        def start(rule):
            rule | ([Parallel(Plus(one), Plus(two), Plus(three))], Star(CHARACTER))
            rule.result = {
                'one': [one],
                'two': [two],
                'three': [three],
            }
        
        def one(rule):
            rule | CHARACTER
        
        def two(rule):
            rule | (CHARACTER, CHARACTER)
        
        def three(rule):
            rule | (CHARACTER, CHARACTER, CHARACTER)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        result = grammar.parse(value)
        iterations = len(value) // 6
        length = iterations * 6
        self.assertEqual(result, {
            'one': [[value[n]] for n in range(length)],
            'two': [[value[n], value[n + 1]] for n in range(0, length, 2)],
            'three': [[value[n], value[n + 1], value[n + 2]] for n in range(0, length, 3)],
        })
    
    @given(first=text(min_size=1), second=text(min_size=1))
    def test_branches(self, first, second):
        # All branches must pass, not just some of them.
        def start(rule):
            rule | Parallel(one, two, three)
        
        def one(rule):
            rule | Plus(CHARACTER)
        
        def two(rule):
            rule | ([tuple(first)], [tuple(second)])
        
        def three(rule):
            rule | (tuple(first), tuple(second))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        grammar.parse(first + second)
        
        with self.assertRaises(ParseError):
            grammar.parse(first)
        
        with self.assertRaises(ParseError):
            grammar.parse(second)
    
    @given(value=text())
    def test_return(self, value):
        # Branches should be returned in the order given.
        def start(rule):
            rule | Parallel(one, two, three)
        
        def one(rule):
            rule | tuple(value)
            rule.translator = lambda node, scope: "one"
        
        def two(rule):
            rule | tuple(value)
            rule.translator = lambda node, scope: "two"
        
        def three(rule):
            rule | tuple(value)
            rule.translator = lambda node, scope: "three"
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(value)
        self.assertEqual(result, ["one", "two", "three"])


class PlusTests(TestCase):
    @given(head=text(), items=text(min_size=1), times=integers(min_value=2, max_value=5), tail=text())
    def test_parsing(self, head, items, times, tail):
        # Plus() rule items can be listed one or more times.
        def start(rule):
            rule | (tuple(head), Plus(*items), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Once
        text = head + items + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Multiple times
        text = head + (items * times) + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Zero times
        with self.assertRaises(ParseError):
            grammar.parse(head + tail)
    
    @given(item=text(min_size=1), times=integers(min_value=2, max_value=5), tail=text())
    def test_greed(self, item, times, tail):
        # Plus() tries to match as many times as possible.
        assume(not tail.startswith(item))
        
        def start(rule):
            rule | (Plus(subrule), Star(CHARACTER))
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | tuple(item)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Once
        result = grammar.parse(item + tail)
        self.assertEqual(result, 1)
        
        # Multiple times
        text = (item * times) + tail
        result = grammar.parse(text)
        self.assertEqual(result, times)
    
    @given(first=text(min_size=1), second=text(min_size=1))
    def test_greed_restriction(self, first, second):
        # Plus() should prefer longer items over more iterations.
        assume(not second.startswith(first))
        
        def start(rule):
            rule | Plus(subrule)
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | (tuple(first), [tuple(second)])
            rule | (tuple(second))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + second + second)
        self.assertEqual(result, 2)
        
        result = grammar.parse(first + second)
        self.assertEqual(result, 1)
    
    @given(first=text(min_size=1), second=text(min_size=1))
    def test_ambiguity(self, first, second):
        # Plus() can return fewer iterations if necessary.
        assume(not second.startswith(first))
        
        def start(rule):
            rule | (Plus(subrule), tuple(first + second))
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | tuple(first)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + first + second)
        self.assertEqual(result, 1)


class SeekTests(TestCase):
    @given(head=text(), skipped=text(), sought=text(min_size=1), tail=text())
    def test_parsing(self, head, skipped, sought, tail):
        assume(sought not in skipped + sought[:-1])
        
        def start(rule):
            rule | (tuple(head), Seek(*sought), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        text = head + skipped + sought + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # When there's nothing to skip, the text should still parse.
        text = head + sought + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Without the sought sequence, the text shouldn't parse.
        with self.assertRaises(ParseError):
            grammar.parse(head + skipped + tail)
        with self.assertRaises(ParseError):
            grammar.parse(head + tail)
        
        # Seek() doesn't keep looking for a second copy of the sought sequence.
        with self.assertRaises(ParseError):
            grammar.parse(head + skipped + sought + skipped + sought + tail)
    
    @given(head=text(), skipped=text(), sought=text(min_size=1), tail=text())
    def test_rule_result(self, head, skipped, sought, tail):
        assume(sought not in skipped + sought[:-1])
        
        def start(rule):
            rule | (tuple(head), seeking, tuple(tail))
            rule.result = seeking
        
        def seeking(rule):
            rule | (Seek(subrule))
            rule.result = {'tokens': [CHARACTER], 'sought': subrule}
        
        def subrule(rule):
            rule | tuple(sought)
            rule.result = {'subrule': [CHARACTER]}
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        text = head + skipped + sought + tail
        result = grammar.parse(text)
        self.assertEqual(result, {'tokens': list(skipped), 'sought': {'subrule': list(sought)}})
    
    @given(head=text(), skipped=text(), sought=text(min_size=1), tail=text(min_size=1))
    def test_sought_alternatives(self, head, skipped, sought, tail):
        # The sought sequence may need to allow multiple parsing possibilities.
        assume(sought not in skipped + sought[:-1])
        
        def start(rule):
            rule | (tuple(head), Seek(seeking), tuple(tail))
            rule.result = seeking
        
        def seeking(rule):
            rule | (tuple(sought), list(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # The sought sequence can include an extra copy of the tail
        text = head + skipped + sought + tail + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(sought + tail))
        
        # It can also skip the tail if it's needed for parsing the rest of the text.
        text = head + skipped + sought + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(sought))
    
    @given(head=text(), before=text(), stop=text(min_size=1), after=text(), sought=text(min_size=1), tail=text())
    def test_break(self, head, before, stop, after, sought, tail):
        assume(sought not in before + stop + after + sought[:-1])
        assume(stop not in before + after + sought + tail)
        
        def start(rule):
            rule | (tuple(head), Seek(*sought, stop=tuple(stop)), tuple(tail))
        
        # Without the stop sequence, the text should parse.
        grammar = Grammar(start=start, tokens=[CHARACTER])
        text = head + before + after + sought + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # With the stop sequence, the text shouldn't parse.
        with self.assertRaises(ParseError):
            grammar.parse(head + before + stop + after + sought + tail)


class SomeTests(TestCase):
    @given(head=text(), items=reordered_items(), tail=text())
    def test_order(self, head, items, tail):
        # Some() rule items may appear in any order.
        inner, reordered, length = items
        
        def start(rule):
            rule | (tuple(head), Some(*reordered), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = head + inner + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
    
    @given(head=text(), items=reordered_items(), tail=text())
    def test_partial(self, head, items, tail):
        # Some() rule items need not all appear.
        inner, reordered, length = items
        
        def start(rule):
            rule | (tuple(head), Some(*reordered), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = head + inner[:length] + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
    
    @given(head=text(), items=text(min_size=2), tail=text())
    def test_optional(self, head, items, tail):
        # Some() rule items aren't completely optional.
        def start(rule):
            rule | (tuple(head), Some(*items), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        text = head + tail
        with self.assertRaises(ParseError):
            grammar.parse(text)
    
    @given(first=characters(), second=characters())
    def test_attributable(self, first, second):
        # Items within Some() can be included in a rule's result.
        def start(rule):
            rule | Some(subrule, second)
            rule.result = {
                'subrule': subrule,
                'token': CHARACTER,
            }
        
        def subrule(rule):
            rule | first
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + second)
        self.assertEqual(result, {'subrule': [first], 'token': second})
        
        result = grammar.parse(second + first)
        self.assertEqual(result, {'subrule': [first], 'token': second})


class StarTests(TestCase):
    @given(head=text(), items=text(min_size=1), times=integers(min_value=2, max_value=5), tail=text())
    def test_parsing(self, head, items, times, tail):
        # Star() rule items can be listed zero or more times.
        def start(rule):
            rule | (tuple(head), Star(*items), tuple(tail))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Once
        text = head + items + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Multiple times
        text = head + (items * times) + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
        
        # Zero times
        text = head + tail
        result = grammar.parse(text)
        self.assertEqual(result, list(text))
    
    @given(item=text(min_size=1), times=integers(min_value=2, max_value=5), tail=text())
    def test_greed(self, item, times, tail):
        # Star() tries to match as many times as possible.
        assume(not tail.startswith(item))
        
        def start(rule):
            rule | (Star(subrule), Star(CHARACTER))
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | tuple(item)
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        
        # Once
        result = grammar.parse(item + tail)
        self.assertEqual(result, 1)
        
        # Multiple times
        text = (item * times) + tail
        result = grammar.parse(text)
        self.assertEqual(result, times)
    
    @given(first=text(min_size=1), second=text(min_size=1))
    def test_greed_restriction(self, first, second):
        # Longer items should be preferred over more iterations.
        assume(not second.startswith(first))
        
        def start(rule):
            rule | Star(subrule)
            rule.result = [subrule]
            rule.translator = lambda node, scope: len(node)
        
        def subrule(rule):
            rule | (tuple(first), [tuple(second)])
            rule | (tuple(second))
        
        grammar = Grammar(start=start, tokens=[CHARACTER])
        result = grammar.parse(first + second + second)
        self.assertEqual(result, 2)


class SequenceTests(TestCase):
    @given(head=text(), item=text(min_size=1), copies=lists(integers(min_value=0, max_value=10), min_size=3, max_size=3), tail=text())
    def test_defaults(self, head, item, copies, tail):
        # Sequence can parse an exact number of the given sequence.
        assume(not tail.startswith(item))
        low, number, high = sorted(copies)
        
        def start(rule):
            rule | (tuple(head), sequenced, Star(CHARACTER))
            rule.result = sequenced
        
        def sequenced(rule):
            rule | Sequence(*item, minimum=number, limit=number)
            rule.translator = lambda node, scope: join_tokens(node)
        
        grammar = Grammar(start, tokens=[CHARACTER])
        
        # With the right number of copies of the sequence
        result = grammar.parse(head + (item * number) + tail)
        self.assertEqual(result, (item * number))
        
        # With too many copies of the sequence
        # (In this case, extra copies get eaten by the tail.)
        result = grammar.parse(head + (item * high) + tail)
        self.assertEqual(result, (item * number))
        
        # With too few copies of the sequence
        if low < number:
            with self.assertRaises(ParseError):
                grammar.parse(head + (item * low) + tail)
        
        # The items in the sequence must be in the right order.
        meti = ''.join(reversed(item))
        if meti != item and number > 0:
            with self.assertRaises(ParseError):
                grammar.parse(head + (meti * number) + tail)
    
    @given(head=text(), item=text(min_size=1), copies=lists(integers(min_value=0, max_value=20), min_size=3, max_size=3), tail=text())
    def test_too_few(self, head, item, copies, tail):
        # Sequences may require a minimum number of iterations.
        assume(not tail.startswith(item))
        low, medium, high = sorted(copies)
        assume(low < medium)
        
        def start(rule):
            rule | (tuple(head), sequenced, Star(CHARACTER))
            rule.result = sequenced
        
        def sequenced(rule):
            rule | Sequence(*item, minimum=medium, limit=high)
            rule.translator = lambda node, scope: join_tokens(node)
        
        grammar = Grammar(start, tokens=[CHARACTER])
        
        text = head + (item * low) + tail
        with self.assertRaises(ParseError):
            grammar.parse(text)
    
    @given(head=text(), item=text(min_size=1), copies=lists(integers(min_value=0, max_value=20), min_size=4, max_size=4), tail=text())
    def test_limited(self, head, item, copies, tail):
        # Within the limits, Sequence parses as many copies as it can find.
        assume(not tail.startswith(item))
        low, medium, high, higher = sorted(copies)
        
        def start(rule):
            rule | (tuple(head), sequenced, Star(CHARACTER))
            rule.result = sequenced
        
        def sequenced(rule):
            rule | Sequence(*item, minimum=low, limit=high)
            rule.translator = lambda node, scope: join_tokens(node)
        
        grammar = Grammar(start, tokens=[CHARACTER])
        
        text = head + (item * medium) + tail
        self.assertEqual(grammar.parse(text), item * medium)
        
        # The Sequence stops parsing once it reaches the limit.
        text = head + (item * higher) + tail
        self.assertEqual(grammar.parse(text), item * high)
    
    @given(head=text(), item=text(min_size=1), copies=lists(integers(min_value=0, max_value=20), min_size=3, max_size=3), tail=text())
    def test_too_many(self, head, item, copies, tail):
        # If the parent rule can't handle any extras, it can throw a ParseError.
        assume(not tail.startswith(item))
        low, medium, high = sorted(copies)
        assume(medium < high)
        
        def start(rule):
            rule | (tuple(head), sequenced, tuple(tail))
            rule.result = sequenced
        
        def sequenced(rule):
            rule | Sequence(*item, minimum=low, limit=medium)
            rule.translator = lambda node, scope: join_tokens(node)
        
        grammar = Grammar(start, tokens=[CHARACTER])
        text = head + (item * high) + tail
        with self.assertRaises(ParseError):
            grammar.parse(text)
    
    @given(head=text(), item=text(min_size=1), copies=integers(min_value=1, max_value=20), tail=text())
    def test_unlimited(self, head, item, copies, tail):
        # A Sequence limit can be set to None to allow as many copies as possible.
        def start(rule):
            rule | (tuple(head), sequenced, tuple(tail))
            rule.result = sequenced
        
        def sequenced(rule):
            rule | Sequence(*item, minimum=1, limit=None)
            rule.translator = lambda node, scope: join_tokens(node)
        
        grammar = Grammar(start, tokens=[CHARACTER])
        text = head + (item * copies) + tail
        self.assertEqual(grammar.parse(text), item * copies)
    
    @given(head=text(), item=text(min_size=1), copies=integers(min_value=1, max_value=20), tail=text())
    def test_forgiving(self, head, item, copies, tail):
        # Sequences will attempt to use fewer copies if necessary to make subsequence parses work.
        def start(rule):
            rule | (tuple(head), sequenced, tuple(item + tail))
            rule.result = sequenced
        
        def sequenced(rule):
            rule | Sequence(*item, minimum=1, limit=None)
            rule.translator = lambda node, scope: join_tokens(node)
        
        grammar = Grammar(start, tokens=[CHARACTER])
        text = head + (item * (copies + 1)) + tail
        self.assertEqual(grammar.parse(text), item * copies)
    
    @given(head=text(min_size=1), tail=text(min_size=1), copies=lists(integers(min_value=1, max_value=20), min_size=3, max_size=3))
    def test_ungreedy(self, head, tail, copies):
        # A Sequence can be set to grab as few copies as possible.
        assume(head not in tail)
        assume(tail not in head)
        first, second, third = copies
        
        class TOKEN(StringToken):
            strings = [head, tail]
        
        def start(rule):
            rule | (sequenced, Star(tail))
            rule.result = sequenced
        
        def sequenced(rule):
            rule | Sequence(TOKEN, minimum=first, limit=None, greedy=False)
            rule.translator = lambda node, scope: len(node)
        
        grammar = Grammar(start, tokens=[TOKEN])
        
        text = (tail * (first + second + third))
        self.assertEqual(grammar.parse(text), first)
        
        text = (tail * first) + (head * second) + (tail * third)
        self.assertEqual(grammar.parse(text), first + second)
        
        text = (head * (first + second + third))
        self.assertEqual(grammar.parse(text), first + second + third)
        
        text = (tail * (first + second)) + (head * third)
        self.assertEqual(grammar.parse(text), first + second + third)
    
    @given(
        head = text(min_size=1),
        tail = text(min_size=1),
        minimum = integers(min_value=0, max_value=20),
        limit = none() | integers(min_value=0, max_value=20),
        greedy = booleans(),
    )
    def test_empty(self, head, tail, minimum, limit, greedy):
        # An empty sequence never parses more than once.
        if limit is not None:
            assume(minimum <= limit)
        
        def start(rule):
            rule | (tuple(head), sequenced, tuple(tail))
            rule.result = sequenced
        
        def sequenced(rule):
            rule | Sequence((), minimum=minimum, limit=limit, greedy=greedy)
        
        grammar = Grammar(start, tokens=[CHARACTER])
        self.assertEqual(grammar.parse(head + tail), [])
    
    @given(item=text(), limits=lists(integers(min_value=0, max_value=20), min_size=2, max_size=2), greedy=booleans())
    def test_invalid_limits(self, item, limits, greedy):
        # A sequence cannot have a limit lower than its minimum value.
        limit, minimum = sorted(limits)
        assume(limit < minimum)
        
        def start(rule):
            rule | Sequence(item, minimum=minimum, limit=limit, greedy=greedy)
        
        with self.assertRaises(RuleError):
            Grammar(start, tokens=[CHARACTER])
