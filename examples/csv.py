from codewhisper.grammar import Grammar
from codewhisper.special import Commas, Notahead, Plus, Star
from codewhisper.tokens import CHARACTER, EOF, CharacterToken, DelimitedToken, StringToken


# Special tokens

class STRING(DelimitedToken):
    start = '"'
    end = '"'
    
    def translate(self, scope):
        return self.value[1:-1]

class SEPARATOR(CharacterToken):
    characters = ','

class NEWLINE(StringToken):
    strings = ['\r\n']


# Rules

def records(rule):
    rule | Commas(row, separator=NEWLINE, trailing=True)
    rule.result = [row]
    def translate_records(node, scope):
        rows = scope.translate(node)
        headers = rows.pop(0)
        return [dict(zip(headers, values)) for values in rows]
    rule.translator = translate_records

def rows(rule):
    rule | [Commas(row, separator=NEWLINE, trailing=True)]
    rule.result = [row]

def row(rule):
    rule | (Notahead(EOF), Commas(cell))
    rule.result = [cell]

def cell(rule):
    rule | Plus(STRING)
    rule | value
    def translate_cell(node, scope):
        return '"'.join(scope.translate(node))
    rule.translator = translate_cell

def value(rule):
    rule | Star(CHARACTER)
    def translate_value(node, scope):
        return ''.join(scope.translate(node))
    rule.translator = translate_value


# Grammars

csv_tokens = [SEPARATOR, NEWLINE, STRING, CHARACTER]

# Parses to a list of lists of strings.
csv_lines = Grammar(start=rows, tokens=csv_tokens)

# Parses to a list of dicts, where the keys are taken from the first row.
csv_records = Grammar(start=records, tokens=csv_tokens)
