# Example parser for TOML: Tom's Obvious, Minimal Language
# Compare and contrast https://github.com/toml-lang/toml/blob/master/toml.abnf
# While normative, that document merely checks for some kinds of validity;
# it does not actually translate a document into a usable form.

from enum import Enum

from codewhisper.grammar import Grammar
from codewhisper.exceptions import InputError
from codewhisper.parsing import Parser
from codewhisper.special import Commas, Notahead, Plus, Star
from codewhisper.tokens import CharacterRangeToken, CharacterToken


# Custom exceptions

class TomlException(InputError):
    pass


# Special tokens

toml_tokens = []

def toml_token(cls):
    toml_tokens.append(cls)
    return cls

@toml_token
class CONTROL(CharacterToken):
    characters = "\t\r\n"

@toml_token
class UPPER(CharacterRangeToken):
    # Upper-case Ascii letters: A-Z
    first = 0x41
    last = 0x5A

@toml_token
class LOWER(CharacterRangeToken):
    # Lower-case Ascii letters: a-z
    first = 0x61
    last = 0x7A

@toml_token
class DIGIT(CharacterRangeToken):
    # Ascii digits: 0-9
    first = 0x30
    last = 0x39

@toml_token
class PRINTABLE(CharacterRangeToken):
    # Other printable Ascii code points
    first = 0x20
    last = 0x7E

@toml_token
class SURROGATE(CharacterRangeToken):
    # UTF-16 Surrogate pair codes
    # Not used, except to exclude them from the UNICODE range.
    first = 0xD800
    last = 0xDFFF

@toml_token
class UNICODE(CharacterRangeToken):
    # Non-Ascii Unicode code points
    first = 0x80
    last = 0x10FFFF


# Common translators

def translate_text(node, scope):
    try:
        return "".join(scope.translate(node))
    except Exception as err:
        raise ValueError(node) from err

def process_text(f, **kwargs):
    def process_translator(node, scope):
        return f("".join(token.value for token in node), **kwargs)
    return process_translator


translate_int = process_text(int)

def translate_table(node, scope):
    result = {}
    defined = {(): Definition.IMPLICIT}
    base_path = ()
    for expression in scope.translate(node):
        if expression is None:
            continue
        
        base = result
        keys, value = expression
        if isinstance(value, Definition):
            path = tuple(keys)
            base_path = path
        else:
            path = base_path + tuple(keys)
        
        for n, key in enumerate(path[:-1], 1):
            partial = path[:n]
            definition = defined.get(partial, Definition.UNDEFINED)
            if definition is Definition.UNDEFINED:
                defined[partial] = Definition.IMPLICIT
                base[key] = {}
            elif definition is Definition.VALUE:
                raise TomlException("Duplicate implicit definition", partial, base[key])
            base = base[key]
            if definition is Definition.ARRAY:
                base = base[-1]
        
        key = path[-1]
        definition = defined.get(path, Definition.UNDEFINED)
        
        if value is Definition.TABLE:
            if definition is Definition.UNDEFINED:
                base[key] = {}
            elif definition is not Definition.IMPLICIT:
                raise TomlException("Duplicate table definition", path, base[key])
            
            defined[path] = value
        elif value is Definition.ARRAY:
            if definition is Definition.UNDEFINED:
                base[key] = [{}]
            elif definition is Definition.ARRAY:
                # Add a new table, and clear any definitions from the old one.
                # It's tempting to put an array index into the path instead,
                # but dotted keys expect to implicitly use the latest one.
                base[key].append({})
                for key in list(defined):
                    if key[:len(path)] == path:
                        defined[key] = Definition.UNDEFINED
            else:
                raise TomlException("Duplicate array definition", path, base[key])
            
            defined[path] = value
        else:
            if defined.get(path, Definition.UNDEFINED) is not Definition.UNDEFINED:
                raise TomlException("Duplicate value definition", path, base[key])
            defined[path] = Definition.VALUE
            base[key] = value
            
            parent = path[:-1]
            if defined.get(parent) is Definition.IMPLICIT:
                # Don't allow future [table] headers to define this table,
                # but do allow them to add new subtables.
                defined[parent] = Definition.TABLE
    return result

def constant(item):
    def translate_constant(node, scope):
        return item
    return translate_constant


# Constant definitions

class Definition(Enum):
    UNDEFINED = 0
    IMPLICIT = 1
    TABLE = 3
    ARRAY = 4
    VALUE = 5


wschar = {" ", "\t"}

escape = "\\"
quotation_mark = '"'
apostrophe = "'"

minus = "-"
plus = "+"
underscore = "_"
digit1_9 = set("123456789")


# Rules

def toml(rule):
    expression = (ws, [{keyval, table}, ws], [comment])
    rule | Commas(expression, separator=newline)
    rule.result = [keyval, table]
    rule.translator = translate_table

def ws(rule):
    rule | Star(wschar)

def newline(rule):
    rule | (["\r"], "\n")
    rule.translator = translate_text

def non_eol(rule):
    rule | "\t" | UPPER | LOWER | DIGIT | PRINTABLE | UNICODE
    rule.single = True

def PrintableExcept(*characters):
    return (Notahead(set(characters)), non_eol)

def comment(rule):
    rule | ("#", Star(non_eol))

def keyval(rule):
    rule | (key, ws, "=", ws, val)
    rule.result = [key, val]

def key(rule):
    rule | Commas(simple_key, separator=(ws, ".", ws))
    rule.result = [simple_key]

def simple_key(rule):
    rule | basic_string | literal_string | unquoted_key
    rule.single = True

def unquoted_key(rule):
    rule | Plus({UPPER, LOWER, DIGIT, "-", "_"})
    rule.translator = translate_text

def val(rule):
    rule | ml_basic_string | basic_string | ml_literal_string | literal_string
    rule | true | false | array | inline_table
    rule | date_time | local_date | local_time
    rule | tfloat | dec_int | hex_int | oct_int | bin_int
    rule.single = True

def basic_string(rule):
    rule | (quotation_mark, Star(basic_char), quotation_mark)
    rule.result = [basic_char]
    rule.translator = translate_text

def basic_char(rule):
    rule | PrintableExcept('"', "\\") | escaped
    rule.single = True

def escaped(rule):
    rule | (escape, escape_seq_char)
    rule.result = escape_seq_char

def escape_seq_char(rule):
    escapes = {
        '"': '"',
        '\\': '\\',
        "b": "\x08",
        "f": "\x0C",
        "n": "\x0A",
        "r": "\x0D",
        "t": "\x09",
    }
    rule | set(escapes)
    rule | ("u", HEXDIG, HEXDIG, HEXDIG, HEXDIG)
    rule | ("U", HEXDIG, HEXDIG, HEXDIG, HEXDIG, HEXDIG, HEXDIG, HEXDIG, HEXDIG)
    def translate_escape(node, scope):
        char, *hexes = node
        if hexes:
            return chr(int("".join(token.value for token in hexes), 16))
        else:
            return escapes[char.value]
    rule.translator = translate_escape

def ml_basic_string(rule):
    ml_basic_string_delim = (quotation_mark, quotation_mark, quotation_mark)
    rule | (ml_basic_string_delim, [newline], ml_basic_body, ml_basic_string_delim)
    rule.result = ml_basic_body

def ml_basic_body(rule):
    mlb_quotes = (quotation_mark, [quotation_mark])
    rule | (Star(mlb_content), Star(mlb_quotes, Plus(mlb_content)), [mlb_quotes])
    rule.translator = translate_text

def mlb_content(rule):
    rule | basic_char | newline | mlb_escaped_nl
    rule.single = True

def mlb_escaped_nl(rule):
    rule | (escape, Plus(ws, newline), ws)
    rule.translator = constant("")

def literal_string(rule):
    rule | (apostrophe, Star(literal_char), apostrophe)
    rule.result = [literal_char]
    rule.translator = translate_text

def literal_char(rule):
    rule | PrintableExcept("'")
    rule.single = True

def ml_literal_string(rule):
    ml_literal_string_delim = (apostrophe, apostrophe, apostrophe)
    rule | (ml_literal_string_delim, [newline], ml_literal_body, ml_literal_string_delim)
    rule.result = ml_literal_body

def ml_literal_body(rule):
    rule | (Star(mll_content), Star(mll_quotes, Plus(mll_content)), [mll_quotes])
    rule.translator = translate_text

def mll_content(rule):
    rule | literal_char | newline
    rule.single = True

def mll_quotes(rule):
    rule | (apostrophe, [apostrophe])
    rule.translator = translate_text


unsigned_dec_int = Parser("0", (digit1_9, Star([underscore], DIGIT)))

def dec_int(rule):
    rule | ([{minus, plus}], unsigned_dec_int)
    rule.translator = translate_int

def hex_int(rule):
    rule | ("0", "x", HEXDIG, Star([underscore], HEXDIG))
    rule.translator = process_text(int, base=16)
def oct_int(rule):
    digits = set("01234567")
    rule | ("0", "o", digits, Star([underscore], digits))
    rule.translator = process_text(int, base=8)
def bin_int(rule):
    digits = set("01")
    rule | ("0", "b", digits, Star([underscore], digits))
    rule.translator = process_text(int, base=2)

def tfloat(rule):
    zero_prefixable_int = (DIGIT, Star([underscore], DIGIT))
    exp = ({"E", "e"}, [{minus, plus}], zero_prefixable_int)
    frac = (".", zero_prefixable_int)

    inf = ("i", "n", "f")
    nan = ("n", "a", "n")
    
    rule | ([{minus, plus}], unsigned_dec_int, Parser(exp, (frac, [exp])))
    rule | ([{minus, plus}], {inf, nan})
    rule.translator = process_text(float)

def true(rule):
    rule | ("t", "r", "u", "e")
    rule.translator = constant(True)
def false(rule):
    rule | ("f", "a", "l", "s", "e")
    rule.translator = constant(False)


def date_fullyear(rule):
    rule | (DIGIT, DIGIT, DIGIT, DIGIT)
    rule.translator = translate_int

def two_digit(rule):
    rule | (DIGIT, DIGIT)
    rule.translator = translate_int


date_month = two_digit  # 01-12
date_mday = two_digit  # 01-28, 01-29, 01-30, 01-31 based on month/year
time_hour = two_digit  # 00-23
time_minute = two_digit  # 00-59
time_second = two_digit  # 00-58, 00-59, 00-60 based on leap second rules

def time_secfrac(rule):
    rule | (".", Plus(DIGIT))
    rule.result = [DIGIT]
    rule.translator = translate_int

def time_offset(rule):
    time_numoffset = ({"+", "-"}, time_hour, ":", time_minute)
    rule | "Z" | "z" | time_numoffset
    rule.result = {
        "numbers": [two_digit],
        "sign": PRINTABLE,
    }
    def translate_timezone(node, scope):
        from datetime import timedelta, timezone
        if not node['numbers']:
            return timezone.utc
        hours, minutes = scope.translate(node['numbers'])
        delta = timedelta(hours=hours, minutes=minutes)
        if node['sign'].value == "-":
            delta = -delta
        return timezone(delta)
    rule.translator = translate_timezone


partial_time = (time_hour, ":", time_minute, ":", time_second, [time_secfrac])
full_date = (date_fullyear, "-", date_month, "-", date_mday)

def date_time(rule):
    rule | (full_date, {"T", "t", " "}, partial_time, [time_offset])
    rule.result = {
        "numbers": [date_fullyear, two_digit, time_secfrac],
        "timezone": time_offset,
    }
    def translate_datetime(node, scope):
        from datetime import datetime
        tz = scope.translate(node['timezone'])
        return datetime(*scope.translate(node['numbers']), tzinfo=tz)
    rule.translator = translate_datetime

def local_date(rule):
    rule | full_date
    rule.result = [date_fullyear, two_digit]
    def translate_date(node, scope):
        from datetime import date
        return date(*scope.translate(node))
    rule.translator = translate_date

def local_time(rule):
    rule | partial_time
    rule.result = [two_digit, time_secfrac]
    def translate_time(node, scope):
        from datetime import time
        return time(*scope.translate(node))
    rule.translator = translate_time


def array(rule):
    ws_comment_newline = Star(Parser(wschar, ([comment], newline)))
    array_values = Commas(ws_comment_newline, val, ws_comment_newline, separator=",", trailing=True)
    rule | ("[", [array_values], ws_comment_newline, "]")
    rule.result = [val]

def table(rule):
    rule | std_table | array_table
    rule.single = True

def std_table(rule):
    rule | ("[", ws, key, ws, "]")
    rule.result = key
    def translate_table_header(node, scope):
        return scope.translate(node), Definition.TABLE
    rule.translator = translate_table_header

def array_table(rule):
    rule | ("[", "[", ws, key, ws, "]", "]")
    rule.result = key
    def translate_array_header(node, scope):
        return scope.translate(node), Definition.ARRAY
    rule.translator = translate_array_header

def inline_table(rule):
    rule | ("{", ws, [Commas(keyval, separator=(ws, ",", ws))], ws, "}")
    rule.result = [keyval]
    rule.translator = translate_table

def HEXDIG(rule):
    rule | DIGIT
    rule | "A" | "B" | "C" | "D" | "E" | "F"
    rule | "a" | "b" | "c" | "d" | "e" | "f"
    rule.single = True


# Grammar

toml_grammar = Grammar(start=toml, tokens=toml_tokens)

loads = toml_grammar.parse
