r'''One way to create a JSON parser with CodeWhisper.
    This grammar relies on a few complex token classes to handle much of the work.
'''#"""#'''


from codewhisper.grammar import Grammar
from codewhisper.special import Commas
from codewhisper.tokens import CharacterToken, DelimitedToken, PatternToken, StringToken


# Special tokens

class CONSTANT(StringToken):
    strings = {
        'true': True,
        'false': False,
        'null': None,
    }
    
    def translate(self, scope):
        return self.strings[self.value]

class NUMBER(PatternToken):
    pattern = r"-?(?:0|[1-9][0-9]*)(?:\.[0-9]+|)(?:[eE][-+]?[0-9]+|)|NaN|-?Infinity"
    initial = "-0123456789"
    
    def translate(self, scope):
        if self.value.lstrip("-").isdigit():
            return int(self.value)
        return float(self.value)

class STRING(DelimitedToken):
    start = '"'
    end = '"'
    escape = '\\'
    
    def translate(self, scope):
        from re import sub
        pattern = r'\\(u[Dd][89ABab][0-9A-Fa-f]{2}\\u[Dd][CDEFcdef][0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|.)'
        escapes = {
            '"': '"',
            '/': '/',
            '\\': '\\',
            'b': '\b',
            'f': '\f',
            'n': '\n',
            'r': '\r',
            't': '\t',
        }
        text = self.value[1:-1]
        if any(ord(c) < 0x20 for c in text):
            raise ValueError("Invalid control character")
        def replacement(match):
            code = match.group(1)
            if code in escapes:
                return escapes[code]
            if len(code) == 5:
                return chr(int(code[1:], 16))
            if len(code) == 11:
                # Recombine surrogate pairs
                return chr((((int(code[2:5], 16) & 0x3FF) << 10) | (int(code[8:11], 16) & 0x3FF)) + 0x10000)
            raise ValueError(f"Invalid escape code {code!r}")
        return sub(pattern, replacement, text)

class SYMBOL(CharacterToken):
    characters = ',:[]{}'

class WHITESPACE(CharacterToken):
    characters = ' \n\r\t'


# Rules

def value(rule):
    rule | STRING | CONSTANT | NUMBER | json_object | json_list
    rule.single = True

def json_object(rule):
    rule | ('{', [Commas(STRING, ':', value)], '}')
    rule.result = {'keys': [STRING], 'values': [value]}
    def object_translator(node, scope):
        return {
            scope.translate(key): scope.translate(value)
            for key, value in zip(node['keys'], node['values'])
        }
    rule.translator = object_translator

def json_list(rule):
    rule | ('[', [Commas(value)], ']')
    rule.result = [value]


# Grammar

grammar = Grammar(start=value, tokens=[SYMBOL, WHITESPACE, CONSTANT, NUMBER, STRING], ignore=[WHITESPACE])

loads = grammar.parse
