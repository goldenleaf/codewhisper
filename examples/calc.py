from codewhisper.grammar import Grammar
from codewhisper.special import Star
from codewhisper.tokens import CharacterToken, KeywordToken, PatternToken


# Special tokens

class COMPLEX(CharacterToken):
    characters = 'ij'

class CONSTANT(KeywordToken):
    strings = [
        "Infinity",
        "infinity",
        "inf",
        "NaN",
        "nan",
    ]
    
    def translate(self, scope):
        return float(self.value)

class NUMBER(PatternToken):
    pattern = r"(?:0|[1-9][0-9]*)(?:\.[0-9]+|)(?:[eE][-+]?[0-9]+|)"
    
    def translate(self, scope):
        return float(self.value)

class SYMBOL(CharacterToken):
    characters = '()*+-/×÷'

class WHITESPACE(CharacterToken):
    characters = ' \n\r\t'


# Rules

def expression(rule):
    rule | (term, Star("+", term), ["-", term])
    rule.result = {
        'operators': [SYMBOL],
        'terms': [term],
    }
    def translate(node, scope):
        result = scope.translate(node['terms'].pop(0))
        for operator, term in zip(node['operators'], node['terms']):
            if operator.value == '+':
                result += scope.translate(term)
            else:
                result -= scope.translate(term)
        return result
    rule.translator = translate

def term(rule):
    rule | (factor, Star({"*", "×"}, factor), [{"/", "÷"}, factor])
    rule.result = {
        'operators': [SYMBOL],
        'factors': [factor],
    }
    def translate(node, scope):
        result = scope.translate(node['factors'].pop(0))
        for operator, factor in zip(node['operators'], node['factors']):
            if operator.value in {"*", "×"}:
                result *= scope.translate(factor)
            else:
                result /= scope.translate(factor)
        return result
    rule.translator = translate

def factor(rule):
    rule | (["-"], value)
    rule.result = {
        'negated': SYMBOL,
        'value': value,
    }
    def translate(node, scope):
        result = scope.translate(node['value'])
        if node['negated'] is not None:
            result = -result
        return result
    rule.translator = translate

def value(rule):
    rule | ("(", expression, ")")
    rule | (NUMBER, [COMPLEX])
    rule | CONSTANT
    rule.result = {
        'complex': COMPLEX,
        'value': {CONSTANT, NUMBER, expression},
    }
    def translate(node, scope):
        result = scope.translate(node['value'])
        if node['complex'] is not None:
            result *= 1j
        return result
    rule.translator = translate


# Grammar

grammar = Grammar(start=expression, tokens=[SYMBOL, WHITESPACE, CONSTANT, COMPLEX, NUMBER], ignore=[WHITESPACE])
