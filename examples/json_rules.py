r'''One way to create a JSON parser with CodeWhisper.
    This grammar relies on a large set of parsing rules to handle most of the work.
'''#"""#'''


from math import inf, nan

from codewhisper.grammar import Grammar
from codewhisper.parsing import Parser
from codewhisper.special import Commas, Notahead, Plus, Star
from codewhisper.tokens import CHARACTER, CharacterRangeToken, CharacterToken, StringToken, Token


# Special tokens

class CONSTANT(StringToken):
    strings = {
        'true': True,
        'false': False,
        'null': None,
        'NaN': nan,
        'Infinity': inf,
        '-Infinity': -inf,
    }
    
    def translate(self, scope):
        return self.strings[self.value]

class CONTROL(CharacterRangeToken):
    first = 0
    last = 0x1F

class DIGIT(CharacterRangeToken):
    first = ord("0")
    last = ord("9")

class HEX(CharacterToken):
    characters = "ABCDEFabcdef"

class SYMBOL(CharacterToken):
    characters = '\\"'

class WHITESPACE(Token):
    characters = ' \n\r\t'
    
    @classmethod
    def match(cls, text, position):
        start = text[position]
        if start not in cls.characters:
            return None
        result = 1
        for c in text[position + 1:]:
            if c not in cls.characters:
                break
            result += 1
        return result


# Rules

def value(rule):
    val = {CONSTANT, json_object, json_list, json_int, json_float, json_string}
    rule | ([WHITESPACE], val, [WHITESPACE])
    rule.result = val

def json_object(rule):
    rule | ('{', [WHITESPACE], [Commas(json_string, [WHITESPACE], ':', value, separator=(",", [WHITESPACE]))], '}')
    rule.result = {'keys': [json_string], 'values': [value]}
    def object_translator(node, scope):
        return {
            scope.translate(key): scope.translate(value)
            for key, value in zip(node['keys'], node['values'])
        }
    rule.translator = object_translator

def json_list(rule):
    rule | ('[', [{Commas(value), WHITESPACE}], ']')
    rule.result = [value]

def json_int(rule):
    rule | (["-"], "0")
    rule | (["-"], Notahead("0"), Plus(DIGIT))
    def int_translator(node, scope):
        return int("".join(token.value for token in node))
    rule.translator = int_translator

def json_float(rule):
    integral = {"0", (Notahead("0"), Plus(DIGIT))}
    fractional = (".", Plus(DIGIT))
    exponential = ({"e", "E"}, [{"-", "+"}], Plus(DIGIT))
    rule | (["-"], integral, Parser((fractional, [exponential]), exponential))
    def float_translator(node, scope):
        return float("".join(token.value for token in node))
    rule.translator = float_translator

def json_string(rule):
    rule | ('"', Star({
        character,
        ("\\", escape),
        ("\\", "u", codepoint),
    }), '"')
    rule.result = [character, escape, codepoint]
    def string_translator(node, scope):
        return "".join(scope.translate(node))
    rule.translator = string_translator

def character(rule):
    # Deliberately excludes CONTROL and SYMBOL
    rule | WHITESPACE | CONSTANT | DIGIT | HEX | CHARACTER
    rule.single = True
    def character_translator(node, scope):
        return node.value
    rule.translator = character_translator

def escape(rule):
    characters = {
        '"': '"',
        '/': '/',
        '\\': '\\',
        'b': '\b',
        'f': '\f',
        'n': '\n',
        'r': '\r',
        't': '\t',
    }
    
    rule | set(characters)
    rule.single = True
    def escape_translator(node, scope):
        return characters[node.value]
    rule.translator = escape_translator

def codepoint(rule):
    hexdigit = {HEX, DIGIT}
    rule | ({"D", "d"}, set("89ABab"), hexdigit, hexdigit, "\\", "u", {"D", "d"}, set("CDEFcdef"), hexdigit, hexdigit)
    rule | (hexdigit, hexdigit, hexdigit, hexdigit)
    rule.result = [HEX, DIGIT]
    def codepoint_translator(node, scope):
        code = "".join(token.value for token in node)
        if len(node) == 4:
            return chr(int(code, 16))
        return chr((((int(code[1:4], 16) & 0x3FF) << 10) | (int(code[5:8], 16) & 0x3FF)) + 0x10000)
    rule.translator = codepoint_translator


# Grammar

grammar = Grammar(start=value, tokens=[WHITESPACE, CONSTANT, CONTROL, DIGIT, HEX, SYMBOL, CHARACTER])

loads = grammar.parse
